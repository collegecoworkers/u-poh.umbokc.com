<?php
require_once 'core/frontinit.php';	

//Get Freelancer's Data
$freelancerid = Input::get('id');
$query = DB::getInstance()->get("freelancer", "*", ["freelancerid" => $freelancerid, "LIMIT" => 1]);
if ($query->count() === 1) {
	foreach($query->results() as $row) {
		$name = $row->name;
		$username = $row->username;
		$email = $row->email;
		$phone = $row->phone;
		$freelancer_imagelocation = $row->imagelocation;
		$freelancer_bgimage = $row->bgimage;
	}
} else {
	Redirect::to('services.php');
}

$query = DB::getInstance()->get("profile", "*", ["userid" => $freelancerid, "LIMIT" => 1]);
if ($query->count()) {
	foreach($query->results() as $row) {
		$nid = $row->id;
		$location = $row->location;
		$rate = $row->rate;
		$website = $row->website;
		$about = $row->about;
		$education_profile = $row->education;
		$work_profile = $row->work;
		$awards_profile = $row->awards;
	}			
} else {
	Redirect::to('services.php');
}

?>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en"> 
<!--<![endif]-->

<!-- Include header.php. Contains header content. -->
<?php include ('includes/template/header.php'); ?> 

<body class="greybg">
	
	<!-- Include navigation.php. Contains navigation content. -->
	<?php include ('includes/template/navigation.php'); ?> 	 
	
		 <!-- ==============================================
	 Header
	 =============================================== -->	 
	 <header class="header-freelancer" style="

	 background: linear-gradient(
	 	rgba(34,34,34,0.7), 
	 	rgba(34,34,34,0.7)
	 	), url('Freelancer/<?= $freelancer_bgimage; ?>') no-repeat center center fixed;
	 background-size: cover;
	 background-position: center center;
	 -webkit-background-size: cover;
	 -moz-background-size: cover;
	 -o-background-size: cover;
	 color: #fff;
	 height: 85vh;
	 width: 100%;
	 
	 display: flex;
	 flex-direction: column;
	 justify-content: center;
	 align-items: center;
	 text-align: center;">
	 <div class="container">
	 	<div class="content">
	 		<div class="row">
	 			<div class="col-lg-12">
	 				<img src="Freelancer/<?= $freelancer_imagelocation; ?>" class="img-thumbnail img-responsive revealOnScroll" data-animation="fadeInDown" data-timeout="200" alt="">
	 				<h1 class="revealOnScroll" data-animation="bounceIn" data-timeout="200"> <?= $name; ?></h1>
	 				<?php
	 				$admin = new Admin();
	 				$client = new Client();
	 				$freelancer = new Freelancer(); 

	 				if ($admin->isLoggedIn()) { 
	 				} elseif($freelancer->isLoggedIn()) {

	 				} elseif($client->isLoggedIn()) {
	 					echo $sen .='	 
	 					<a href="Client/invite.php?id='. escape($freelancerid) .'" class="kafe-btn kafe-btn-mint-small">
	 					<i class="fa fa-align-left"></i> ' . $lang['Предложить работу'] . '</a>
	 					';
	 				} else {
	 					echo $sen .='	 
	 					<a href="login.php" class="kafe-btn kafe-btn-mint-small">
	 					<i class="fa fa-align-left"></i> ' . $lang['Предложить работу'] . '</a>
	 					';
	 				}
	 				?> 		  
	 			</div><!-- /.col-lg-12 -->
	 		</div><!-- /.row -->
	 	</div><!-- /.content -->
	 </div><!-- /.container -->
	</header><!-- /header -->
		<!-- ==============================================
		Overview Section
		=============================================== -->
		
		<section class="overview" id="overview">
			<div class="container">
				<div class="row">
					
					<div id="sidebar" class="col-lg-4">
						<div class="list">
							<div class="list-group">
								
								<a class="list-group-item active cat-list">
									<em class="fa fa-fw fa-user"></em>&nbsp;&nbsp;&nbsp;<?= $lang['freelancer']; ?>
								</a>
								
							</div><!-- ./.list-group -->
						</div><!-- ./.list --> 
						
						<?php $overview = (Input::get('a') == 'overview') ? ' active' : ''; ?>
						<?php $portfolio = (Input::get('a') == 'portfolio') ? ' active' : ''; ?>
						<?php $jobs = (Input::get('a') == 'jobs') ? ' active' : ''; ?>
						<div class="list">
							<div class="list-group">
								
								<a href="freelancer.php?a=overview&id=<?= $freelancerid ?>" class="list-group-item <?= $overview; ?> cat-list">
									<em class="fa fa-fw fa-align-justify"></em>&nbsp;&nbsp;&nbsp;<?= $lang['Обо мне']; ?>
								</a>
								<a href="freelancer.php?a=portfolio&id=<?= $freelancerid ?>" class="list-group-item <?= $portfolio; ?> cat-list">
									<em class="fa fa-fw fa-align-justify"></em>&nbsp;&nbsp;&nbsp;<?= $lang['Портфолио']; ?>
								</a>
								<a href="freelancer.php?a=jobs&id=<?= $freelancerid ?>" class="list-group-item <?= $jobs; ?> cat-list">
									<em class="fa fa-fw fa-align-justify"></em>&nbsp;&nbsp;&nbsp;<?= $lang['Завершенные & назначенные работы']; ?>
								</a>
							</div><!-- ./.list-group -->
						</div><!-- ./.list --> 
					</div><!-- ./.col-lg-4 -->


					<?php if (Input::get('a') == 'overview') : ?>		 
						<div class="col-lg-8 white-2">
							<div class="about">
								<h3><?= $lang['Обо мне']; ?></h3>
								<div class="col-lg-12 top-sec">
									<?= $about; ?>
									
									<?php
									foreach ($arr as $key => $value) {
										echo '<label class="label label-success">'. $value .'</label> &nbsp;'; 
									}
									?>		   
								</div><!-- /.col-lg-12 --> 	
								
								<div class="row bottom-sec">
									
									<div class="col-lg-12">
										
										<div class="col-lg-12">
											<hr class="small-hr">
										</div><!-- /.col-lg-12 --> 
										<div class="col-lg-3">
											<h5><?= $lang['Выполняемые работы']; ?> </h5>
											<p>
												<?php	
												$query = DB::getInstance()->get("job", "*", ["AND" => ["freelancerid" => $freelancerid, "accepted" => 1]]);
												echo $query->count();
												?>	
											</p>
										</div><!-- /.col-lg-2 -->
										<div class="col-lg-3">
											<h5><?= $lang['Завершенные работы']; ?> </h5>
											<p>
												<?php	
												$query = DB::getInstance()->get("job", "*", ["AND" => ["freelancerid" => $freelancerid, "completed" => 1]]);
												echo $query->count();
												?>	
											</p>
										</div><!-- /.col-lg-2 -->
									</div><!-- /.col-lg-12 -->
									
									<div class="col-lg-12">
										
										<div class="col-lg-12">
											<hr class="small-hr">
										</div><!-- /.col-lg-12 --> 
									</div><!-- /.col-lg-12 -->		   

									<div class="col-lg-12">

										<div class="col-lg-12">
											<hr class="small-hr">
										</div><!-- /.col-lg-12 --> 

										<div class="col-lg-4">
											<h5> <?= $lang['website']; ?> </h5>
											<p><?= $website; ?></p>
										</div><!-- /.col-lg-3 -->
										<div class="col-lg-2">
											<h5> <?= $lang['Ставка/час']; ?> </h5>
											<p><?= $currency_symbol; ?> <?= $rate; ?></p>
										</div><!-- /.col-lg-1 -->
										<div class="col-lg-3">
											<h5> <?= $lang['phone']; ?> </h5>
											<p><i class="fa fa-phone"></i> <?= $phone; ?></p>
										</div><!-- /.col-lg-3 -->
										<div class="col-lg-3">
											<h5> <?= $lang['email']; ?> </h5>
											<p> <?= $email; ?></p>
										</div><!-- /.col-lg-3 -->

									</div><!-- /.col-lg-12 -->
								</div><!-- /.col-lg-12 -->
							</div><!-- /.about -->

							<div class="education">		  
								<h3><?= $lang['education']; ?></h3>
								<div class="row">
									<div class="col-lg-12">
										<div class="col-md-12">
											<?= $education_profile; ?>
										</div><!-- /.col-lg-12 -->
									</div><!-- /.col-lg-12 -->  
								</div><!-- /.row -->

							</div><!-- Education-->

							<div class="work">		  
								<h3><?= $lang['work']; ?> <?= $lang['experience']; ?></h3>

								<div class="row">
									<div class="col-lg-12">
										<div class="col-lg-12">
											<?= $work_profile; ?>
										</div><!-- /.col-lg-12 -->
									</div> <!-- /.col-lg-12 --> 
								</div><!-- /.row -->

							</div><!-- Work-->

							<div class="awards">		  
								<h3><?= $lang['awards']; ?> <?= $lang['and']; ?> <?= $lang['achievements']; ?></h3>

								<div class="row">
									<div class="col-lg-12">
										<div class="col-lg-12">
											<?= $awards_profile; ?>
										</div><!-- /.col-lg-12 -->
									</div><!-- /.col-lg-12 -->
								</div><!-- /.row -->

							</div><!-- Awards-->
							<?php

							$query = DB::getInstance()->get("freelancer", "*", ["freelancerid" => $freelancerid, "LIMIT" => 1]);
							if ($query->count()) {
								foreach($query->results() as $row) {	        
									$membershipid = $row->membershipid; 

								}
							}	

							$q = DB::getInstance()->get("membership_freelancer", "*", ["membershipid" => $membershipid]);
							if ($q->count() === 1) {
								$q1 = DB::getInstance()->get("membership_freelancer", "*", ["membershipid" => $membershipid]);
							} else {
								$q1 = DB::getInstance()->get("membership_agency", "*", ["membershipid" => $membershipid]);
							}
							if ($q1->count()) {
								foreach($q1->results() as $r1) {
									$team_membership = $r1->team;
								}
							} 		 		 
							?>		

							<?php if($team_membership === '1'): ?>

								<div class="ourteam">		  
									<h3><?= $lang['our']; ?> <?= $lang['team']; ?></h3>

									<div class="row">
										<div class="col-lg-12">

											<?php
											$query = DB::getInstance()->get("team", "*", ["userid" => $freelancerid]);
											if ($query->count()) {

												$teamList = '';
												$x = 1;	

												foreach($query->results() as $row) {

													echo $teamList .= '

													<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 profile">
													<div class="img-box">
													<img src="Freelancer/'. escape($row->imagelocation) .'" class="img-responsive" alt="">
													<ul class="text-center">
													<li><a href="'. escape($row->facebook) .'" target="_blank"><i class="fa fa-facebook"></i></a></li>
													<li><a href="'. escape($row->twitter) .'" target="_blank"><i class="fa fa-twitter"></i></a></li>
													<li><a href="'. escape($row->linkedin) .'" target="_blank"><i class="fa fa-linkedin"></i></a></li>
													</ul>
													</div>
													<h4>'. escape($row->name) .'</h4>
													<h5>'. escape($row->title) .'</h5>
													<p>'. escape($row->description) .'</p>
													</div><!-- /.col-lg-4 -->			
													';

													unset($teamList);	 
													$x++;								
												}
											} else {
												echo $teamList .='';
											}		   


											?>				

										</div><!-- /.col-lg-12 -->  
									</div><!-- /.row -->
								</div><!-- Awards-->			  		    
							<? endif; ?>
						</div><!-- /.col-lg-8 -->

					<?php elseif (Input::get('a') == 'portfolio') : ?>

						<div class="col-lg-8 white-2" id="portfolio">

							<div class="row">
								<div class="col-lg-12">
									<h3><?= $lang['Портфолио']; ?></h3>
								</div><!-- /.col-lg-12 -->
							</div><!-- /.row -->
							<br/> 

							<div class="row">
								<div class="col-lg-12">

									<?php
									$query = DB::getInstance()->get("portfolio", "*", ["userid" => $freelancerid]);
									if ($query->count()) {

										$portfolioList = '';
										$x = 1;	

										foreach($query->results() as $row) {

											$portfolio_id = $row->id;
											$portfolio_title = $row->title;
											$portfolio_date = $row->date;
											$portfolio_client = $row->client;	
											$portfolio_website = $row->website;	
											$portfolio_desc = $row->description;	
											$portfolio_imagelocation = $row->imagelocation;						

											echo $portfolioList .= '

											<div class="col-sm-6 portfolio-item">
											<a href="#project-modal-'.$portfolio_id.'" class="portfolio-link" data-toggle="modal">
											<div class="caption">
											<div class="caption-content">
											<i class="fa fa-search-plus fa-3x"></i>
											</div><!-- /.caption-content -->
											</div><!-- /.caption -->
											<img src="Freelancer/'. escape($row->imagelocation) .'" class="img-responsive" alt="" />
											</a>
											</div><!-- /.col-lg-6 -->					  		
											';
											unset($portfolioList);	 
											$x++;								
										}
									} else {
										echo $portfolioList .='<h3>'.$lang['no_content_found'].'</h3>';
									}		   


									?>		   	

								</div><!-- ./col-lg-12--> 
							</div><!-- Row-->		  


						</div><!-- ./col-lg-8--> 	

					<?php elseif (Input::get('a') == 'jobs') : ?>		 
						<div class="col-lg-8 white-2 jobslist">
							<?php		


							$page = (int) (!isset($_GET["page"]) ? 1 : $_GET["page"]);
							$limit = $job_limit;
							$startpoint = ($page * $limit) - $limit;	  

							$q1 = DB::getInstance()->get("job", "*", ["freelancerid" => $freelancerid]);
							$total = $q1->count();


							$query = DB::getInstance()->get("job", "*", ["ORDER" => "date_added DESC", "LIMIT" => [$startpoint, $limit],
								"AND" => [
									"freelancerid" => $freelancerid,
								]]);
							if($query->count()) {

								$jobList = '';
								$x = 1;	


								foreach($query->results() as $row) {
									$jobList = '';

									$q1 = DB::getInstance()->get("client", "*", ["clientid" => $row->clientid]);
									if ($q1->count()) {
										foreach ($q1->results() as $r1) {
											$name1 = $r1->name;	
											$username1 = $r1->username;	
											$imagelocation = $r1->imagelocation;	
										}
									}	

									$q2 = DB::getInstance()->get("proposal", "*", ["jobid" => $row->jobid]);
									if ($q2->count() === 0) {
										$job_proposals = 0;	
									} else {
										$job_proposals = $q2->count();
									}			

									$blurb = truncateHtml($row->description, 400);		
									if ($row->accepted === '1' ) {
										if ($row->completed === '1') {
											$senp .='
											<p>' . $lang['completed'] . '</p>
											';	
										} else {
											$senp .='
											<p>' . $lang['in_complete'] . '</p>
											';	
										}
									} else {
										$senp .='
										<p>' . $lang['waiting'] . ' ' . $lang['freelancer'] . ' ' . $lang['to'] . ' ' . $lang['accept'] . '</p>
										';	
									}


									echo $jobList .= '
									<div class="job">	

									<div class="row top-sec">
									<div class="col-lg-12">
									<div class="col-lg-2 col-xs-12">
									<a href="freelancer.html">
									<img class="img-responsive" src="Client/'. escape($imagelocation) .'" alt="">
									</a>
									</div><!-- /.col-lg-2 -->
									<div class="col-lg-10 col-xs-12"> 
									<h4><a href="jobpost.php?title='. escape($row->slug) .'">'. escape($row->title) .'</a></h4>
									<h5><a href="client.php?a=overview&id='. escape($row->clientid) .'" 
									style="text-decoration: none !important; color: #05CB95 !important;">
									'. escape($name1) .'</a> <small>@'. escape($username1) .'</small></h5>
									</div><!-- /.col-lg-10 -->

									</div><!-- /.col-lg-12 -->
									</div><!-- /.row -->

									<div class="row mid-sec">			 
									<div class="col-lg-12">			 
									<div class="col-lg-12">
									<hr class="small-hr">
									'. $blurb .'
									</div><!-- /.col-lg-12 -->
									</div><!-- /.col-lg-12 -->
									</div><!-- /.row -->

									<div class="row bottom-sec">
									<div class="col-lg-12">

									<div class="col-lg-12">
									<hr class="small-hr">
									</div> 

									<div class="col-lg-2">
									<h5>' . $lang['Опубликованно'] . ' </h5>
									<p>'. ago(strtotime($row->date_added)) .'</p>
									</div>
									<div class="col-lg-2">
									<h5> ' . $lang['Бюджет'] . '</h5>
									<p>$'. escape($row->budget) .'</p>
									</div>
									<div class="col-lg-2">
									<h5>' . $lang['Заявки'] . '</h5>
									<p>'. escape($job_proposals) .'</p>
									</div>

									</div><!-- /.col-lg-12 -->
									</div><!-- /.row -->

									</div><!-- /.job -->
									';

									unset($jobList); 
									unset($senp);		
									$x++;		 
								}
							}else {
								echo $jobList = '<p>'.$lang['no_content_found'].'</p>';
							}

							echo Pagination($total,$limit,$page,'?a=jobs&id='.$clientid.'&');
							?>

						</div><!-- /.col-lg-8 -->

					<?php endif; ?>         
				</div><!-- /.row -->

			</div><!-- /.container --> 
		</section><!-- End section-->	 

		<!-- Include footer.php. Contains footer content. -->	
		<?php include 'includes/template/footer.php'; ?>	

		 <!-- ==============================================
	 PROJECT PREVIEW MODAL (Do not alter this markup)
	 =============================================== -->

	 <?php
	 $query = DB::getInstance()->get("portfolio", "*", ["userid" => $freelancerid]);
	 if ($query->count()) {

	 	$portfolioList = '';
	 	$x = 1;	

	 	foreach($query->results() as $row) {

	 		$portfolio_id = $row->id;
	 		$portfolio_title = $row->title;
	 		$portfolio_date = $row->date;
	 		$portfolio_client = $row->client;	
	 		$portfolio_website = $row->website;	
	 		$portfolio_desc = $row->description;	
	 		$portfolio_imagelocation = $row->imagelocation;						

	 		?>
	 		<div id="project-modal-<?= $portfolio_id ?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	 			<div class="modal-dialog">
	 				<div class="modal-content">
	 					<div class="modal-header">
	 						<div class="container">
	 							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	 							<h1 id="hdr-title" class="text-center"><?= $portfolio_title; ?></h1>
	 							<div class="row">
	 								<div class="col-md-8 col-md-offset-2 text-center">
	 									<div class="image-wrapper">
	 										<img class="img-responsive" src="Freelancer/<?= $portfolio_imagelocation; ?>" alt="">
	 									</div>
	 									<!--./image-wrapper -->
	 								</div><!--./col-md-8 -->
	 							</div><!--./row -->
	 						</div><!--./container -->
	 					</div><!--./modal-header -->
	 					<div class="modal-body">
	 						<div class="container">
	 							<div class="row">
	 								<div id="project-sidebar" class="col-md-3">
	 									<h3><?= $portfolio_title; ?></h3>
	 									<p><i class="fa fa-calendar"></i><?= $portfolio_date; ?></p>
	 									<p><i class="fa fa-user"></i><?= $portfolio_client; ?></p>
	 									<p><i class="fa fa-globe"></i> <a href="http://<?= $portfolio_website; ?>" target="_blank"><?= $portfolio_website; ?></a></p>
	 								</div>
	 								<div id="project-content" class="col-md-9">
	 									<?= $portfolio_desc; ?>
	 									<p><a class="kafe-btn kafe-btn-mint-small" href="http://<?= $portfolio_website; ?>" target="_blank">Visit Website <i class="fa fa-arrow-right"></i></a></p>
	 								</div>
	 							</div>
	 						</div>
	 					</div><!-- End modal-body -->
	 				</div><!-- End modal-content -->
	 			</div><!-- End modal-dialog -->
	 		</div><!-- End modal -->	   
	 		<?php

	 		unset($portfolioList);	 
	 		$x++;								
	 	}
	 } else {
	 	echo $portfolioList .='<h3>'.$lang['no_content_found'].'</h3>';
	 }		   


	 ?>		   	

		 <!-- ==============================================
	 Scripts
	 =============================================== -->
	 
	 <!-- jQuery 2.1.4 -->
	 <script src="/assets/js/jQuery-2.1.4.min.js"></script>
	 <!-- Bootstrap 3.3.6 JS -->
	 <script src="/assets/js/bootstrap.min.js" type="text/javascript"></script>

	 <script src="/assets/js/jquery.knob.js"></script>
	 <script src="/assets/js/knob.js"></script>
	 <!-- Waypoints JS -->
	 <script src="/assets/js/kafe/waypoints.min.js" type="text/javascript"></script>
	 <!-- Kafe JS -->
	 <script src="/assets/js/kafe.js" type="text/javascript"></script>

	</body>
	</html>
