<?
$freelancer = new Freelancer();
$freelancerid = $freelancer->data()->freelancerid;

$basename = basename($_SERVER["REQUEST_URI"], ".php");
$editname = basename($_SERVER["REQUEST_URI"]);
?>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">

		<!-- Sidebar user panel (optional) -->
		<div class="user-panel">
			<div class="pull-left image">
				<img src="<?= escape($freelancer->data()->imagelocation); ?>" class="img-circle" alt="User Image" />
			</div>
			<div class="pull-left info">
				<p><?= escape($freelancer->data()->name); ?></p>
				<!-- Status -->
				<a href="profile.php?a=profile"><i class="fa fa-circle text-success"></i> <?= $lang['online']; ?></a>
			</div>
		</div>


		<!-- Sidebar Menu -->
		<ul class="sidebar-menu">
			<li class="header"><?= $lang['sidenav_header_2']; ?></li>
			<!-- Optionally, you can add icons to the links -->
			<li class="<?= $active = ($basename == 'index') ? 'active' : ''; ?>">
				<a href="index.php"><i class='fa fa-dashboard'></i> <span><?= $lang['dashboard']; ?></span></a>
			</li>     
			<li class="treeview<?php 
			echo $active = ($basename == 'joblist') ? ' active' : '';   ?>">
			<a href="joblist.php"><i class='fa fa-align-left'></i> <span><?= $lang['Работы']; ?></span></a>
		</li>        
		<li class="treeview<?php 
		echo $active = ($basename == 'jobinvite') ? ' active' : '';  echo $active = ($editname == 'viewinvite.php?id='. Input::get('id').'') ? ' active' : ''; ?>">
		<a href="jobinvite.php"><i class='fa fa-filter'></i> <span><?= $lang['Приглашения']; ?></span>
			<span class="label label-info pull-right">
				<?php 	
				$q1 = DB::getInstance()->get("job", "*", ["AND" => ["freelancerid" => $freelancer->data()->freelancerid, "invite" => "1"]]);
				echo $q1->count();
				?></span></a>
			</li>     
			<li class="treeview<?php 
			echo $active = ($basename == 'proposallist') ? ' active' : ''; echo $active = ($editname == 'addproposal.php?id='. Input::get('id').'') ? ' active' : ''; echo $active = ($editname == 'editproposal.php?id='. Input::get('id').'') ? ' active' : ''; echo $active = ($editname == 'viewproposal.php?id='. Input::get('id').'') ? ' active' : ''; ?>">
			<a href="proposallist.php"><i class='fa  fa-files-o'></i> <span><?= $lang['Предложения']; ?></span></a>
		</li>     
		<li class="<?= $active = ($basename == 'jobassigned') ? ' active' : ''; echo $active = ($editname == 'jobboard.php?a='. Input::get('a').'&id='. Input::get('id').'') ? ' active' : '';?>">
			<a href="jobassigned.php"><i class='fa fa-address-card'></i> <span><?= $lang['Назначения']; ?></span></a>
		</li>    
		<li class="header"><?= $lang['Информация профиля']; ?></li>          
		<li class="treeview<?php 
		echo $active = ($basename == 'overview') ? ' active' : '';   ?>">
		<a href="profile.php?a=profile"><i class='fa fa-info-circle'></i> <span><?= $lang['Профиль']; ?></span></a>
	</li> 
		<li class="treeview<?php 
		echo $active = ($basename == 'overview') ? ' active' : '';   ?>">
		<a href="overview.php?a=profile"><i class='fa fa-info-circle'></i> <span><?= $lang['Страница']; ?></span></a>
	</li> 
	<li class="treeview<?= $active = ($basename == 'portfoliolist') ? ' active' : ''; echo $active = ($basename == 'addportfolio') ? ' active' : ''; echo $active = ($editname == 'editportfolio.php?id='. Input::get('id').'') ? ' active' : ''; ?>">
		<a href="#"><i class='fa fa-briefcase'></i> <span><?= $lang['Потрфолио']; ?></span> <i class="fa fa-angle-left pull-right"></i></a>
		<ul class="treeview-menu">
			<li><a href="portfoliolist.php"><?= $lang['list']; ?></a></li>
			<li><a href="addportfolio.php"><?= $lang['add']; ?></a></li>
		</ul>
	</li>
	</ul><!-- /.sidebar-menu -->
</section>
<!-- /.sidebar -->
</aside>