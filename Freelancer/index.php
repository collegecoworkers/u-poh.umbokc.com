<?php 
require_once '../core/init.php';	

//Start new Freelancer object
$freelancer = new Freelancer();

//Check if Freelancer is logged in
if (!$freelancer->isLoggedIn()) {
	Redirect::to('../index.php');	
}

?>
<!DOCTYPE html>
<html lang="en-US" class="no-js">

<!-- Include header.php. Contains header content. -->
<?php include ('template/header.php'); ?> 
<!-- Panel CSS -->
<link href="../assets/css/AdminLTE/AdminLTE.min.css" rel="stylesheet" type="text/css" />	     

<body class="skin-green sidebar-mini">

		 <!-- ==============================================
		 Wrapper Section
		 =============================================== -->
		 <div class="wrapper">

		 	<!-- Include navigation.php. Contains navigation content. -->
		 	<?php include ('template/navigation.php'); ?> 
		 	<!-- Include sidenav.php. Contains sidebar content. -->
		 	<?php include ('template/sidenav.php'); ?> 

		 	<!-- Content Wrapper. Contains page content -->
		 	<div class="content-wrapper">
		 		<!-- Content Header (Page header) -->
		 		<section class="content-header">
		 			<h1><?php echo $lang['dashboard']; ?><small><?php echo $lang['section']; ?></small></h1>
		 			<ol class="breadcrumb">
		 				<li><a href="index.php"><i class="fa fa-dashboard"></i> <?php echo $lang['home']; ?></a></li>
		 				<li class="active"><?php echo $lang['dashboard']; ?></li>
		 			</ol>
		 		</section>

		 		<!-- Main content -->
		 		<section class="content">
		 			<!-- Include currency.php. Contains header content. -->
		 			<?php include ('template/currency.php'); ?> 

		 			<!-- Info boxes -->
		 			<div class="row">
		 				<div class="col-md-4 col-sm-6 col-xs-12">
		 					<div class="info-box">
		 						<span class="info-box-icon bg-aqua"><i class="fa fa-calendar"></i></span>
		 						<div class="info-box-content">
		 							<span class="info-box-text">Сейчас</span>
		 							<span class="info-box-number small">
		 								<script type="text/javascript" language="JavaScript">
		 									var calendarDate = getCalendarDate();
		 									document.write(calendarDate);
		 								</script>

		 								<form name="clock" disabled="disabled">
		 									<input type="submit" class="trans" name="face" value="">
		 								</form>		
		 								<style type="text/css">
		 								input[name=face] {
		 									pointer-events: none;
		 									tab-index: -1;
		 								}
		 							</style>	
		 						</span>
		 					</div><!-- /.info-box-content -->
		 				</div><!-- /.info-box -->
		 			</div><!-- /.col -->

		 			<div class="col-md-4 col-sm-6 col-xs-12">
		 				<div class="info-box">
		 					<span class="info-box-icon bg-olive"><i class="fa fa-align-left"></i></span>
		 					<div class="info-box-content">
		 						<span class="info-box-text"><?php echo $lang['Доступные работы']; ?></span>
		 						<span class="info-box-number small">
		 							<?php	
		 							$query = DB::getInstance()->get("job", "*", ["AND" => ["invite" => 0, "completed" => 0]]);
		 							echo $query->count();
		 							?>	
		 						</span>
		 					</div><!-- /.info-box-content -->
		 				</div><!-- /.info-box -->
		 			</div><!-- /.col -->

		 			<!-- fix for small devices only -->
		 			<div class="clearfix visible-sm-block"></div>

		 			<div class="col-md-4 col-sm-6 col-xs-12">
		 				<div class="info-box">
		 					<span class="info-box-icon bg-olive"><i class="fa fa-align-right"></i></span>
		 					<div class="info-box-content">
		 						<span class="info-box-text"><?php echo $lang['Завершенные работы']; ?></span>
		 						<span class="info-box-number">
		 							<?php	
		 							$query = DB::getInstance()->get("job", "*", ["AND" => ["freelancerid" => $freelancer->data()->freelancerid, "completed" => 1]]);
		 							echo $query->count();
		 							?>	
		 						</span>
		 					</div><!-- /.info-box-content -->
		 				</div><!-- /.info-box -->
		 			</div><!-- /.col -->
		 		</div><!-- /.row -->  

		 	</section><!-- /.content -->
		 </div><!-- /.content-wrapper -->

		 <?php include 'template/footer.php'; ?>	

		</div>
		 <!-- ==============================================
	 Scripts
	 =============================================== -->

	 <!-- jQuery 2.1.4 -->
	 <script src="../assets/js/jQuery-2.1.4.min.js"></script>
	 <!-- Bootstrap 3.3.6 JS -->
	 <script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
	 <!-- AdminLTE App -->
	 <script src="../assets/js/app.min.js" type="text/javascript"></script>  

	 <script type="text/javascript" language="JavaScript">
	 	<!-- Copyright 2002 Bontrager Connection, LLC

	 	function getCalendarDate()
	 	{
	 		var months = new Array(13);
	 		months[0]  = "Январь";
	 		months[1]  = "Февраль";
	 		months[2]  = "Март";
	 		months[3]  = "Апрель";
	 		months[4]  = "Мочь";
	 		months[5]  = "Июнь";
	 		months[6]  = "Июль";
	 		months[7]  = "Август";
	 		months[8]  = "Сентябрь";
	 		months[9]  = "Октябрь";
	 		months[10] = "Ноябрь";
	 		months[11] = "Декабрь";
	 		var now         = new Date();
	 		var monthnumber = now.getMonth();
	 		var monthname   = months[monthnumber];
	 		var monthday    = now.getDate();
	 		var year        = now.getYear();
	 		if(year < 2000) { year = year + 1900; }
	 		var dateString = monthname +
	 		' ' +
	 		monthday +
	 		', ' +
	 		year;
	 		return dateString;
	} // function getCalendarDate()
	//-->
</script>	
<script language="javascript" type="text/javascript">
	/* Visit http://www.yaldex.com/ for full source code
	and get more free JavaScript, CSS and DHTML scripts! */
	<!-- Begin
	var timerID = null;
	var timerRunning = false;
	function stopclock (){
		if(timerRunning)
			clearTimeout(timerID);
		timerRunning = false;
	}
	function showtime () {
		var now = new Date();
		var hours = now.getHours();
		var minutes = now.getMinutes();
		var seconds = now.getSeconds()
		var timeValue = "" + ((hours >12) ? hours -12 :hours)
		if (timeValue == "0") timeValue = 12;
		timeValue += ((minutes < 10) ? ":0" : ":") + minutes
		timeValue += ((seconds < 10) ? ":0" : ":") + seconds
		timeValue += (hours >= 12) ? " P.M." : " A.M."
		document.clock.face.value = timeValue;
		timerID = setTimeout("showtime()",1000);
		timerRunning = true;
	}
	function startclock() {
		stopclock();
		showtime();
	}
	window.onload=startclock;
	// End -->
</script>     

<script src="../assets/js/jquery.knob.min.js"></script>
<script>
	$(function($) {

		$(".knob").knob({
			change : function (value) {
										//console.log("change : " + value);
									},
									release : function (value) {
										//console.log(this.$.attr('value'));
										console.log("release : " + value);
									},
									cancel : function () {
										console.log("cancel : ", this);
									},
									format : function (value) {
										return value + '%';
									},
									draw : function () {

										// "tron" case
										if(this.$.data('skin') == 'tron') {

											this.cursorExt = 0.3;

												var a = this.arc(this.cv)  // Arc
														, pa                   // Previous arc
														, r = 1;

														this.g.lineWidth = this.lineWidth;

														if (this.o.displayPrevious) {
															pa = this.arc(this.v);
															this.g.beginPath();
															this.g.strokeStyle = this.pColor;
															this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, pa.s, pa.e, pa.d);
															this.g.stroke();
														}

														this.g.beginPath();
														this.g.strokeStyle = r ? this.o.fgColor : this.fgColor ;
														this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, a.s, a.e, a.d);
														this.g.stroke();

														this.g.lineWidth = 2;
														this.g.beginPath();
														this.g.strokeStyle = this.o.fgColor;
														this.g.arc( this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
														this.g.stroke();

														return false;
													}
												}
											});

						// Example of infinite knob, iPod click wheel
						var v, up=0,down=0,i=0
						,$idir = $("div.idir")
						,$ival = $("div.ival")
						,incr = function() { i++; $idir.show().html("+").fadeOut(); $ival.html(i); }
						,decr = function() { i--; $idir.show().html("-").fadeOut(); $ival.html(i); };
						$("input.infinite").knob(
						{
							min : 0
							, max : 20
							, stopper : false
							, change : function () {
								if(v > this.cv){
									if(up){
										decr();
										up=0;
									}else{up=1;down=0;}
								} else {
									if(v < this.cv){
										if(down){
											incr();
											down=0;
										}else{down=1;up=0;}
									}
								}
								v = this.cv;
							}
						});
					});
				</script>         

				<!-- ChartJS 1.0.1 -->
				<script src="../assets/plugins/chartjs/Chart.min.js" type="text/javascript"></script>
				<script type="text/javascript">
					$(function () {
	//-----------------------
	//- MONTHLY SALES CHART -
	//-----------------------

	// Get context with jQuery - using jQuery's .get() method.
	var salesChartCanvas = $("#salesChart").get(0).getContext("2d");
	// This will get the first returned node in the jQuery collection.
	var salesChart = new Chart(salesChartCanvas);

	var salesChartData = {
		labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
		datasets: [
		{
			label: "Electronics",
			fillColor: "rgb(255, 99, 132)",
			lineTension: 0.1,
			strokeColor: "rgb(255, 99, 132)",
			pointColor: "rgb(255, 99, 132)",
			pointStrokeColor: "#c1c7d1",
			pointHighlightFill: "#fff",
			pointHighlightStroke: "rgb(220,220,220)",
			data : <?=json_encode(array_values($monthvalues));?>
		},
		{
			label: "Digital Goods",
			fillColor: "rgba(60,141,188,0.9)",
			strokeColor: "rgba(60,141,188,0.8)",
			pointColor: "#3b8bba",
			pointStrokeColor: "rgba(60,141,188,1)",
			pointHighlightFill: "#fff",
			pointHighlightStroke: "rgba(60,141,188,1)",
			data: <?=json_encode(array_values($monthvalue));?>
		}
		]
	};

	var salesChartOptions = {
		//Boolean - If we should show the scale at all
		showScale: true,
		scaleLabel:
		function(label){return  ' $ ' + label.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");},
		//Boolean - Whether grid lines are shown across the chart
		scaleShowGridLines: false,
		//String - Colour of the grid lines
		scaleGridLineColor: "rgba(0,0,0,.05)",
		//Number - Width of the grid lines
		scaleGridLineWidth: 1,
		//Boolean - Whether to show horizontal lines (except X axis)
		scaleShowHorizontalLines: true,
		//Boolean - Whether to show vertical lines (except Y axis)
		scaleShowVerticalLines: true,
		//Boolean - Whether the line is curved between points
		bezierCurve: true,
		//Number - Tension of the bezier curve between points
		bezierCurveTension: 0.3,
		//Boolean - Whether to show a dot for each point
		pointDot: true,
		//Number - Radius of each point dot in pixels
		pointDotRadius: 4,
		//Number - Pixel width of point dot stroke
		pointDotStrokeWidth: 1,
		//Number - amount extra to add to the radius to cater for hit detection outside the drawn point
		pointHitDetectionRadius: 20,
		//Boolean - Whether to show a stroke for datasets
		datasetStroke: true,
		//Number - Pixel width of dataset stroke
		datasetStrokeWidth: 2,
		//Boolean - Whether to fill the dataset with a color
		datasetFill: true,
		//String - A legend template
		//Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
		maintainAspectRatio: false,
		//Boolean - whether to make the chart responsive to window resizing
		responsive: true
	};

	//Create the line chart
	salesChart.Line(salesChartData, salesChartOptions);		
	
});

</script>

</body>
</html>