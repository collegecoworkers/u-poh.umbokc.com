<?php 
require_once '../core/init.php';	

$freelancer = new Freelancer();

//Check if Freelancer is logged in
if (!$freelancer->isLoggedIn()) {
	Redirect::to('../index.php');	
}

//Getting Job Data
$jobid = Input::get('id');
$query = DB::getInstance()->get("job", "*", ["jobid" => $jobid, "LIMIT" => 1]);
if ($query->count() === 1) {
	foreach($query->results() as $row) {
		$clientid = $row->clientid;
		$freelancerid = $row->freelancerid;
		$jobid = $row->jobid;
		$catid = $row->catid;
		$job_title = $row->title;
		$job_budget = $row->budget;
		$job_description = $row->description;
		$job_start_date = $row->start_date;
		$job_end_date = $row->end_date;
		$job_completed = $row->completed;
	}
} else {
	Redirect::to('joblist.php');
}	

$q1 = DB::getInstance()->get("client", "*", ["clientid" => $clientid, "LIMIT" => 1]);
if ($q1->count()) {
	foreach($q1->results() as $r1) {
		$client_name = $r1->name;
		$client_imagelocation = $r1->imagelocation;
	}			
}

$q2 = DB::getInstance()->get("category", "*", ["catid" => $catid, "LIMIT" => 1]);
if ($q2->count()) {
	foreach($q2->results() as $r2) {
		$category_name = $r2->name;
	}			
}

$q3 = DB::getInstance()->get("proposal", "*", ["AND" => ["jobid" => $jobid, "freelancerid" => $freelancerid], "LIMIT" => 1]);
if ($q3->count()) {
	foreach($q3->results() as $r3) {
		$proposal_budget = $r3->budget;
	}			
}

?>
<!DOCTYPE html>
<html lang="en-US" class="no-js">

<!-- Include header.php. Contains header content. -->
<?php include ('template/header.php'); ?> 
<!-- AdminLTE CSS -->
<link href="../assets/css/AdminLTE/AdminLTE.min.css" rel="stylesheet" type="text/css" />
<!-- Datetime Picker -->
<link href="../assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<!-- Jquery UI CSS -->
<link href="../assets/css/jquery-ui-1.10.3.custom.css" rel="stylesheet" type="text/css" />
<!-- Progress CSS -->
<link href="../assets/css/progress.css" rel="stylesheet" type="text/css" />

<body class="skin-green sidebar-mini">

		 <!-- ==============================================
		 Wrapper Section
		 =============================================== -->
		 <div class="wrapper">

		 	<!-- Include navigation.php. Contains navigation content. -->
		 	<?php include ('template/navigation.php'); ?> 
		 	<!-- Include sidenav.php. Contains sidebar content. -->
		 	<?php include ('template/sidenav.php'); ?> 

		 	<!-- Content Wrapper. Contains page content -->
		 	<div class="content-wrapper">

		 		<!-- Main content -->
		 		<section class="content">	
		 			<!-- Include currency.php. Contains header content. -->
		 			<?php include ('template/currency.php'); ?>  	
		 			<div class="row">	

		 				<div class="col-lg-12 m-t-sm">	
		 					<div class="col-sm-12 m-b-xs">
		 						<span><?php echo escape($job_title); ?></span>
		 						<div class="btn-group pull-right">
		 							<?php if($job_completed === '1'): ?>			
		 								<a class="btn btn-sm btn-danger"> 
		 									<?php echo $lang['job']; ?> <?php echo $lang['завершена']; ?>	
		 								</a>
		 							<?php else: ?>		
		 								<a class="btn btn-sm btn-success"> 
		 									<i class="fa fa-spinner"></i> &nbsp; <?php echo $lang['job']; ?> <?php echo $lang['не завершена']; ?>	
		 								</a>
		 							<?php endif; ?>
		 						</div>
		 					</div>


		 				</div>

		 				<div class="col-lg-12 nav-bg">
		 					<?php $ov = (Input::get('a') == 'overview') ? ' active' : ''; ?>
		 					<?php $di = (Input::get('a') == 'discussions') ? ' active' : ''; ?>
		 					<div class="sub-tab" style="margin-bottom:10px;">
		 						<ul class="nav pro-nav-tabs nav-tabs-dashed">
		 							<li class="<?php echo $ov; ?>"><a href="jobboard.php?a=overview&id=<?php echo $jobid ?>"><?php echo $lang['Обзор']; ?></a></li>
		 							<li class="<?php echo $di; ?>"><a href="jobboard.php?a=discussions&id=<?php echo $jobid ?>"><?php echo $lang['discussions']; ?></a></li>
		 						</ul>
		 					</div>		 	
		 				</div>	
		 			</div><!-- /.row -->

		 			<div class="row proj-summary-band nav-bg">


		 				<div class="col-md-3 text-center">
		 					<label class="small text-muted"><?php echo $lang['Название']; ?></label>
		 					<h4><strong><?php echo escape($job_title); ?></strong></h4>
		 				</div>
		 				
		 				<div class="col-md-3 text-center">
		 					<label class="text-muted small"><?php echo $lang['Бюджет']; ?></label>
		 					<h4><strong>
		 						<?php echo $currency_symbol; ?> <?php echo escape($job_budget); ?>	
		 					</strong></h4>
		 				</div>

		 				<div class="col-md-3 text-center">
		 					<label class="text-muted small"><?php echo $lang['Бюджет фрилансера']; ?></label>
		 					<h4><strong>
		 						<?php echo $currency_symbol; ?> <?php echo escape($proposal_budget); ?>	
		 					</strong></h4>
		 				</div>

		 			</div>        

		 			<br/>
		 			<div class="row">
		 				<?php if (Input::get('a') == 'overview') : ?>
		 					<div class="col-lg-4">
		 						<div class="box box-info">
		 							<div class="box-header">
		 								<h3 class="box-title"><?php echo $lang['Клиент']; ?></h3>
		 							</div><!-- /.box-header -->
		 							<div class="box-body">
		 								<h4 class="text-center"><strong><?php echo $client_name; ?></strong></h4>
		 								<div class="form-group">
		 									<div class="image text-center">
		 										<img src="../Client/<?php echo escape($client_imagelocation); ?>" class="img-thumbnail" width="215" height="215"/>
		 									</div>
		 								</div>

		 							</div><!-- /.box-body -->
		 						</div><!-- /.box -->	         	
		 					</div>	


		 					<div class="col-lg-8">
		 						
		 						<section class="panel panel-default">
		 							<header class="panel-heading"><?php echo $lang['details']; ?></header>
		 							<ul class="list-group no-radius">
		 								<li class="list-group-item">
		 									<span class="pull-right text"><?php echo escape($job_title); ?></span>
		 									<span class="text-muted"><?php echo $lang['Название']; ?></span>
		 								</li>
		 								<li class="list-group-item">
		 									<?php if($job_completed === '1'): ?>		
		 										<span class="pull-right text"><?php echo $lang['Завершена']; ?></span>
		 									<?php else: ?>		
		 										<span class="pull-right text"><?php echo $lang['Не Завершена']; ?></span>
		 									<?php endif; ?>
		 									<span class="text-muted"><?php echo $lang['Завершена']; ?></span>
		 								</li>
		 								<li class="list-group-item">
		 									<span class="pull-right"><a href="../client.php?a=overview&&id=<?php echo escape($clientid); ?>" target="_blank">
		 										<?php echo escape($client_name); ?></a></span>
		 										<span class="text-muted"><?php echo $lang['Клиент']; ?></span>
		 									</li>
		 									<li class="list-group-item">
		 										<span class="pull-right"><?php echo escape($category_name); ?></span>
		 										<span class="text-muted"><?php echo $lang['category']; ?></span>
		 									</li>
		 									<li class="list-group-item">
		 										<span class="pull-right"><?php echo $currency_symbol; ?> <?php echo escape($job_budget); ?></span>
		 										<span class="text-muted"><?php echo $lang['budget']; ?></span>
		 									</li>
		 									<li class="list-group-item">
		 										<span class="pull-right"><?php echo escape($job_start_date); ?></span>
		 										<span class="text-muted"><?php echo $lang['Дата начала']; ?></span>
		 									</li>
		 									<li class="list-group-item">
		 										<span class="pull-right"><?php echo escape($job_end_date); ?></span>
		 										<span class="text-muted"><?php echo $lang['Дата конца']; ?></span>
		 									</li>
		 								</ul>
		 							</section>	

		 							<section class="panel panel-default">
		 								<header class="panel-heading"><?php echo $lang['Ваше предложение']; ?></header>
		 								<ul class="list-group no-radius">
		 									<li class="list-group-item">
		 										<span class="pull-right"><?php echo $currency_symbol; ?> <?php echo escape($proposal_budget); ?></span>
		 										<span class="text-muted"> <?php echo $lang['budget']; ?></span>
		 									</li>
		 								</ul>
		 							</section>   


		 						</div>
		 					</div>

		 				<?php elseif (Input::get('a') == 'discussions') : ?> 
		 					<div class="col-lg-12">		 	
		 						<div class="row">	
		 							<div class="col-md-12">

		 								<?php
		 								$query = DB::getInstance()->get("client", "*", ["clientid" => $clientid, "LIMIT" => 1]);
		 								if($query->count()) {

		 									$x = 1;
		 									foreach($query->results() as $row) {
		 										$messageList = '';

		 										echo $messageList .= '

		 										<div class="col-md-8 col-lg-offset-2">

		 										<!-- Input addon -->
		 										<div class="box box-info">
		 										<div class="box-header">
		 										<h3 class="box-title">' . $lang['discussions'] . ' ' . $lang['between'] . ' ' . $lang['you'] . ' ' . $lang['and'] . '  '. escape($row->name) .'</h3>
		 										</div>
		 										<div class="box-footer bg-body-light">

		 										<div class="form-group message-reply-container">	
		 										<textarea onclick="showMsgButton('. $row->id .')" id="summernote" placeholder="Post Message" class="form-control"></textarea>
		 										</div>
		 										<div id="message_btn_'. $row->id .'" class="comment-btn">
		 										<a onclick="postDiscussion('. $row->id .', '. $freelancer->data()->freelancerid .', '.$clientid.')" class="btn btn-primary">Отправить</a>
		 										</div>
		 										</div>
		 										<div class="box-body">

		 										<div id="messages-list'.$row->id.'" class="post-comments" style="overflow-y: scroll; height: 600px; width: 100%;">
		 										'.getDiscussionF($clientid, $freelancer->data()->freelancerid).'
		 										</div>


		 										</div><!-- /.box-body -->
		 										</div><!-- /.box -->

		 										</div><!-- /.col -->

		 										';

		 										unset($messageList);	 
		 										$x++;		 
		 									}
		 								}else {
		 									echo $messageList = '<p>'.$lang['no_content_found'].'</p>';
		 								}
		 								?>		 			


		 							</div><!-- /.col-lg-12 -->	 
		 						</div><!-- /.row -->	
		 					</div>

		 				<?php endif; ?>		 	              

		 			</section><!-- /.content -->
		 		</div><!-- /.content-wrapper -->

		 		<!-- Include footer.php. Contains footer content. -->	
		 		<?php include 'template/footer.php'; ?>	

		 	</div><!-- /.wrapper -->   
	<!-- ==============================================
	 Scripts
	 =============================================== -->
	 
	 <!-- jQuery 2.1.4 -->
	 <script src="../assets/js/jQuery-2.1.4.min.js"></script>
	 <!-- Bootstrap 3.3.6 JS -->
	 <script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
	 <!-- Jquery UI 1.10.3 -->
	 <script src="../assets/js/jquery-ui-1.10.3.custom.min.js"></script>
	 <!-- UI Slider Progress -->
	 <script src="../assets/js/ui-sliders-progress.js" type="text/javascript"></script>

	 <script src="../assets/js/jquery.knob.min.js"></script>
	 <script src="../assets/js/knob.js"></script>    
	 <!-- DATA TABES SCRIPT -->
	 <script src="../assets/plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
	 <script src="../assets/plugins/datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>
	 <!-- AdminLTE App -->
	 <script src="../assets/js/app.min.js" type="text/javascript"></script>
	 <!-- Ratings JS -->
	 <script src="../assets/js/ratings.js" type="text/javascript"></script>
	 <!-- page script -->
	 <script type="text/javascript">
	 	$(function () {
	 		$("#example1").dataTable({
	 			/* No ordering applied by DataTables during initialisation */
	 			"order": []
	 		});
	 		$("#example2").dataTable({
	 			/* No ordering applied by DataTables during initialisation */
	 			"order": []
	 		});
	 		$("#example3").dataTable({
	 			/* No ordering applied by DataTables during initialisation */
	 			"order": []
	 		});
	 	});
	 </script>
	 <!-- Datetime Picker -->
	 <script src="../assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
	 <script type="text/javascript">
	 	$('.form_datetime_start').datetimepicker({
	 		language:  'ru',
	 		weekStart: 1,
	 		todayBtn:  1,
	 		autoclose: 1,
	 		todayHighlight: 1,
	 		startView: 2,
	 		forceParse: 0,
	 		showMeridian: 1, 
	 		startDate: new Date(),
	 		pickTime: false, 
	 		minView: 2,      
	 		pickerPosition: "bottom-left",
	 		linkField: "mirror_field_start",
	 		linkFormat: "hh:ii",
	 		linkFieldd: "mirror_field_start_date",
	 		linkFormatt: "dd MM yyyy"
	 	});
	 	$('.form_datetime_end').datetimepicker({
	 		language:  'ru',
	 		weekStart: 1,
	 		todayBtn:  1,
	 		autoclose: 1,
	 		todayHighlight: 1,
	 		startView: 2,
	 		forceParse: 0,
	 		showMeridian: 1, 
	 		startDate: new Date(),
	 		pickTime: false, 
	 		minView: 2,      
	 		pickerPosition: "bottom-left",
	 		linkField: "mirror_field_start",
	 		linkFormat: "hh:ii",
	 		linkFieldd: "mirror_field_start_date",
	 		linkFormatt: "dd MM yyyy"
	 	});
	 </script> 

	 <script type="text/javascript">

	 	function showMsgButton(id) {
	 		$('#message_btn_'+id).fadeIn('slow');
	 	}   
	 	function postDiscussion(id, freelancerid, clientid) {
	 		var message = $('#summernote').val();

	 		$('#post_comment_'+id).html('<div class="preloader-retina-large preloader-center"></div>');

	 		$('#message_btn_'+id).fadeOut('slow');

	 		$.ajax({
	 			type: "POST",
	 			url: "template/requests/post_discussion.php",
	 			data: "clientid="+clientid+"&freelancerid="+freelancerid+"&message="+encodeURIComponent(message)+"&id=<?= $_GET['id'] ?>" ,
	 			cache: false,
	 			success: function(html) {
	 				$('#post_comment_'+id).html('');

	 				$('#messages-list'+id).prepend(html);

	 				$('.message-reply-container').fadeIn(500);

	 				$("#summernote").summernote("reset");			
	 			}
	 		});
	 	}

	 	function delete_the(id, freelancerid) {
	 		$('#del_comment_'+id).html('<div class="preloader-retina"></div>');

	 		$.ajax({
	 			type: "POST",
	 			url: "template/requests/delete.php",
	 			data: "id="+id+"&freelancerid="+freelancerid, 
	 			cache: false,
	 			success: function(html) {
	 				if(html == '1') {
	 					$('#comment'+id).fadeOut(500, function() { $('#comment'+id).remove(); });
	 				} else {
	 					$('#comment'+id).html($('#del_comment_'+id).html('Sorry, the message could not be removed, please refresh the page and try again.'));
	 				}
	 			}
	 		});
	 	}

	 </script> 
	</body>
	</html>
