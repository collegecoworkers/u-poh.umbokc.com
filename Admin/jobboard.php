<?php 

require_once '../core/init.php';	

//Start new Admin Object
$admin = new Admin();

//Check if Admin is logged in
if (!$admin->isLoggedIn()) {
	Redirect::to('index.php');	
}


//Getting Job Data
$jobid = Input::get('id');
$query = DB::getInstance()->get("job", "*", ["jobid" => $jobid, "LIMIT" => 1]);
if ($query->count() === 1) {
	foreach($query->results() as $row) {
		$clientid = $row->clientid;
		$freelancerid = $row->freelancerid;
		$jobid = $row->jobid;
		$catid = $row->catid;
		$job_title = $row->title;
		$job_type = $row->job_type;
		$job_budget = $row->budget;
		$job_description = $row->description;
		$job_start_date = $row->start_date;
		$job_end_date = $row->end_date;
		$job_completed = $row->completed;
	}
} else {
	Redirect::to('joblist.php');
}	

$q1 = DB::getInstance()->get("freelancer", "*", ["freelancerid" => $freelancerid, "LIMIT" => 1]);
if ($q1->count()) {
	foreach($q1->results() as $r1) {
		$freelancer_name = $r1->name;
		$freelancer_imagelocation = $r1->imagelocation;
	}			
}

$q2 = DB::getInstance()->get("category", "*", ["catid" => $catid, "LIMIT" => 1]);
if ($q2->count()) {
	foreach($q2->results() as $r2) {
		$category_name = $r2->name;
	}			
}

$q3 = DB::getInstance()->get("proposal", "*", ["AND" => ["jobid" => $jobid, "freelancerid" => $freelancerid], "LIMIT" => 1]);
if ($q3->count()) {
	foreach($q3->results() as $r3) {
		$proposal_budget = $r3->budget;
	}			
}
?>
<!DOCTYPE html>
<html lang="en-US" class="no-js">

<!-- Include header.php. Contains header content. -->
<?php include ('template/header.php'); ?> 
<!-- AdminLTE CSS -->
<link href="../assets/css/AdminLTE/AdminLTE.min.css" rel="stylesheet" type="text/css" />
<!-- Datetime Picker -->
<link href="../assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />

<body class="skin-green sidebar-mini">
	
		 <!-- ==============================================
		 Wrapper Section
		 =============================================== -->
		 <div class="wrapper">
		 	
		 	<!-- Include navigation.php. Contains navigation content. -->
		 	<?php include ('template/navigation.php'); ?> 
		 	<!-- Include sidenav.php. Contains sidebar content. -->
		 	<?php include ('template/sidenav.php'); ?> 
		 	
		 	<!-- Content Wrapper. Contains page content -->
		 	<div class="content-wrapper">

		 		<!-- Main content -->
		 		<section class="content">	
		 			<!-- Include currency.php. Contains header content. -->
		 			<?php include ('template/currency.php'); ?>  	
		 			<div class="row">	
		 				
		 				<div class="col-lg-12 m-t-sm">
		 					<div class="col-sm-12 m-b-xs">
		 						<span><?php echo escape($job_title); ?></span>
		 						<div class="btn-group pull-right">
		 							<?php if($job_completed === '1'): ?>			
		 								<a class="btn btn-sm btn-danger"> 
		 									<?php echo $lang['job']; ?> <?php echo $lang['completed']; ?>			
		 								</a>
		 							<?php else: ?>		
		 								<a class="btn btn-sm btn-success"> 
		 									<i class="fa fa-spinner"></i> &nbsp; <?php echo $lang['job']; ?> <?php echo $lang['не завершена']; ?>			
		 								</a>
		 							<?php endif; ?>
		 						</div>
		 					</div>
		 					
		 					
		 				</div>
		 				
		 				<div class="col-lg-12 nav-bg">
		 					<?php $ov = (Input::get('a') == 'overview') ? ' active' : ''; ?>
		 					<?php $di = (Input::get('a') == 'discussions') ? ' active' : ''; ?>
		 					<div class="sub-tab" style="margin-bottom:10px;">
		 						<ul class="nav pro-nav-tabs nav-tabs-dashed">
		 							<li class="<?php echo $ov; ?>"><a href="jobboard.php?a=overview&id=<?php echo $jobid ?>"><?php echo $lang['Обзор']; ?></a></li>
		 							<li class="<?php echo $di; ?>"><a href="jobboard.php?a=discussions&id=<?php echo $jobid ?>"><?php echo $lang['discussions']; ?></a></li>
		 						</ul>
		 					</div>		 	
		 				</div>	
		 			</div><!-- /.row -->
		 			
		 			<div class="row proj-summary-band nav-bg">

		 				<div class="col-md-3 text-center">
		 					<label class="small text-muted"><?php echo $lang['Название']; ?></label>
		 					<h4><strong><?php echo escape($job_title); ?></strong></h4>
		 				</div>
		 				
		 				<div class="col-md-3 text-center">
		 					<label class="text-muted small"><?php echo $lang['Бюджет']; ?></label>
		 					<h4><strong>
		 						<?php echo $currency_symbol; ?> <?php echo escape($job_budget); ?>	
		 					</strong></h4>
		 				</div>

		 				<div class="col-md-3 text-center">
		 					<label class="text-muted small"><?php echo $lang['Бюджет фрилансера']; ?></label>
		 					<h4><strong>
		 						<?php echo $currency_symbol; ?> <?php echo escape($proposal_budget); ?>	
		 					</strong></h4>
		 				</div>

		 			</div>        

		 			<br/>
		 			<div class="row">
		 				<?php if (Input::get('a') == 'overview') : ?>
		 					<div class="col-lg-4">
		 						<div class="box box-info">
		 							<div class="box-header">
		 								<h3 class="box-title"><?php echo $lang['freelancer']; ?></h3>
		 							</div><!-- /.box-header -->
		 							<div class="box-body">
		 								<h4 class="text-center"><strong><?php echo $freelancer_name; ?></strong></h4>
		 								<div class="form-group">
		 									<div class="image text-center">
		 										<img src="../Freelancer/<?php echo escape($freelancer_imagelocation); ?>" class="img-thumbnail" width="215" height="215"/>
		 									</div>
		 								</div>
		 								
		 							</div><!-- /.box-body -->
		 						</div><!-- /.box -->	         	
		 					</div>	

		 					<div class="col-lg-8">
		 						
		 						<section class="panel panel-default">
		 							<header class="panel-heading"><?php echo $lang['details']; ?></header>
		 							<ul class="list-group no-radius">
		 								<li class="list-group-item">
		 									<span class="pull-right text"><?php echo escape($job_title); ?></span>
		 									<span class="text-muted"><?php echo $lang['Название']; ?></span>
		 								</li>
		 								<li class="list-group-item">
		 									<?php if($job_completed === '1'): ?>		
		 										<span class="pull-right text"><?php echo $lang['Завершена']; ?></span>
		 									<?php else: ?>		
		 										<span class="pull-right text"><?php echo $lang['Не Завершена']; ?></span>
		 									<?php endif; ?>
		 									<span class="text-muted"><?php echo $lang['Завершена']; ?></span>
		 								</li>
		 								<li class="list-group-item">
		 									<span class="pull-right"><a href="../freelancer.php?a=overview&&id=<?php echo escape($freelancerid); ?>" target="_blank">
		 										<?php echo escape($freelancer_name); ?></a></span>
		 										<span class="text-muted"><?php echo $lang['freelancer']; ?></span>
		 									</li>
		 									<li class="list-group-item">
		 										<span class="pull-right"><?php echo escape($category_name); ?></span>
		 										<span class="text-muted"><?php echo $lang['category']; ?></span>
		 									</li>
		 									<li class="list-group-item">
		 										<span class="pull-right"><?php echo $currency_symbol; ?> <?php echo escape($job_budget); ?></span>
		 										<span class="text-muted"><?php echo $lang['budget']; ?></span>
		 									</li>
		 									<li class="list-group-item">
		 										<span class="pull-right"><?php echo escape($job_start_date); ?></span>
		 										<span class="text-muted"><?php echo $lang['Дата начала']; ?></span>
		 									</li>
		 									<li class="list-group-item">
		 										<span class="pull-right"><?php echo escape($job_end_date); ?></span>
		 										<span class="text-muted"><?php echo $lang['Дата конца']; ?></span>
		 									</li>
		 								</ul>
		 							</section>	

		 							<section class="panel panel-default">
		 								<header class="panel-heading"><?php echo $lang['Предложение фрилансера']; ?></header>
		 								<ul class="list-group no-radius">
		 									<li class="list-group-item">
		 										<span class="pull-right"><?php echo $currency_symbol; ?> <?php echo escape($proposal_budget); ?></span>
		 										<span class="text-muted"> <?php echo $lang['budget']; ?></span>
		 									</li>
		 								</ul>
		 							</section>   


		 						</div>
		 					</div>
		 				<?php elseif (Input::get('a') == 'discussions') : ?> 
		 					<div class="col-lg-12">		 	
		 						<div class="row">	
		 							<div class="col-md-12">
		 								
		 								<?php
		 								$query = DB::getInstance()->get("freelancer", "*", ["freelancerid" => $freelancerid, "LIMIT" => 1]);
		 								if($query->count()) {
		 									
		 									$x = 1;
		 									foreach($query->results() as $row) {
		 										$messageList = '';
		 										
		 										echo $messageList .= '
		 										
		 										<div class="col-md-8 col-lg-offset-2">
		 										
		 										<!-- Input addon -->
		 										<div class="box box-info">
		 										<div class="box-header">
		 										<h3 class="box-title">' . $lang['discussions'] . ' ' . $lang['between'] . ' ' . $lang['клиентом'] . ' ' . $lang['и'] . ' ' . $lang['фрилансером'] . '</h3>
		 										</div>
		 										<div class="box-footer bg-body-light">
		 										
		 										</div>
		 										<div class="box-body">
		 										
		 										<div id="messages-list'.$row->id.'" class="post-comments" style="overflow-y: scroll; height: 600px; width: 100%;">
		 										'.getAdminDiscussion($freelancerid, $clientid).'
		 										</div>
		 										
		 										
		 										</div><!-- /.box-body -->
		 										</div><!-- /.box -->
		 										
		 										</div><!-- /.col -->
		 										
		 										';
		 										
		 										unset($messageList);	 
		 										$x++;		 
		 									}
		 								}else {
		 									echo $messageList = '<p>'.$lang['no_content_found'].'</p>';
		 								}
		 								?>		 			
		 								
		 								
		 							</div><!-- /.col-lg-12 -->	 
		 						</div><!-- /.row -->	
		 					</div>
		 				<?php endif; ?>

		 			</section><!-- /.content -->
		 		</div><!-- /.content-wrapper -->

		 		<!-- Include footer.php. Contains footer content. -->	
		 		<?php include 'template/footer.php'; ?>	

		 	</div><!-- /.wrapper -->   
	<!-- ==============================================
	 Scripts
	 =============================================== -->
	 
	 <!-- jQuery 2.1.4 -->
	 <script src="../assets/js/jQuery-2.1.4.min.js"></script>
	 <!-- Bootstrap 3.3.6 JS -->
	 <script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>

	 <script src="../assets/js/jquery.knob.min.js"></script>
	 <script>
	 	$(function($) {

	 		$(".knob").knob({
	 			change : function (value) {
										//console.log("change : " + value);
									},
									release : function (value) {
										//console.log(this.$.attr('value'));
										console.log("release : " + value);
									},
									cancel : function () {
										console.log("cancel : ", this);
									},
									format : function (value) {
										return value + '%';
									},
									draw : function () {

										// "tron" case
										if(this.$.data('skin') == 'tron') {

											this.cursorExt = 0.3;

												var a = this.arc(this.cv)  // Arc
														, pa                   // Previous arc
														, r = 1;

														this.g.lineWidth = this.lineWidth;

														if (this.o.displayPrevious) {
															pa = this.arc(this.v);
															this.g.beginPath();
															this.g.strokeStyle = this.pColor;
															this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, pa.s, pa.e, pa.d);
															this.g.stroke();
														}

														this.g.beginPath();
														this.g.strokeStyle = r ? this.o.fgColor : this.fgColor ;
														this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, a.s, a.e, a.d);
														this.g.stroke();

														this.g.lineWidth = 2;
														this.g.beginPath();
														this.g.strokeStyle = this.o.fgColor;
														this.g.arc( this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
														this.g.stroke();

														return false;
													}
												}
											});

						// Example of infinite knob, iPod click wheel
						var v, up=0,down=0,i=0
						,$idir = $("div.idir")
						,$ival = $("div.ival")
						,incr = function() { i++; $idir.show().html("+").fadeOut(); $ival.html(i); }
						,decr = function() { i--; $idir.show().html("-").fadeOut(); $ival.html(i); };
						$("input.infinite").knob(
						{
							min : 0
							, max : 20
							, stopper : false
							, change : function () {
								if(v > this.cv){
									if(up){
										decr();
										up=0;
									}else{up=1;down=0;}
								} else {
									if(v < this.cv){
										if(down){
											incr();
											down=0;
										}else{down=1;up=0;}
									}
								}
								v = this.cv;
							}
						});
					});
				</script>    
				<!-- DATA TABES SCRIPT -->
				<script src="../assets/plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
				<script src="../assets/plugins/datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>
				<!-- AdminLTE App -->
				<script src="../assets/js/app.min.js" type="text/javascript"></script>
				<!-- Ratings JS -->
				<script src="../assets/js/ratings.js" type="text/javascript"></script>
				<script type="text/javascript">
				</script>
				<!-- page script -->
				<script type="text/javascript">
					$(function () {
						$("#example1").dataTable({
							/* No ordering applied by DataTables during initialisation */
							"order": []
						});
						$("#example2").dataTable({
							/* No ordering applied by DataTables during initialisation */
							"order": []
						});
						$("#example3").dataTable({
							/* No ordering applied by DataTables during initialisation */
							"order": []
						});
					});
				</script>
				<!-- Datetime Picker -->
				<script src="../assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
				<script type="text/javascript">
					$('.form_datetime_start').datetimepicker({
						language:  'ru',
						weekStart: 1,
						todayBtn:  1,
						autoclose: 1,
						todayHighlight: 1,
						startView: 2,
						forceParse: 0,
						showMeridian: 1, 
						startDate: new Date(),
						pickTime: false, 
						minView: 2,      
						pickerPosition: "bottom-left",
						linkField: "mirror_field_start",
						linkFormat: "hh:ii",
						linkFieldd: "mirror_field_start_date",
						linkFormatt: "dd MM yyyy"
					});
					$('.form_datetime_end').datetimepicker({
						language:  'ru',
						weekStart: 1,
						todayBtn:  1,
						autoclose: 1,
						todayHighlight: 1,
						startView: 2,
						forceParse: 0,
						showMeridian: 1, 
						startDate: new Date(),
						pickTime: false, 
						minView: 2,      
						pickerPosition: "bottom-left",
						linkField: "mirror_field_start",
						linkFormat: "hh:ii",
						linkFieldd: "mirror_field_start_date",
						linkFormatt: "dd MM yyyy"
					});
				</script>    
				<script type="text/javascript">
					$(function() {
						
						
						$(".btn-file").click(function(){
							
	//Save the link in a variable called element
	var element = $(this);
	
	//Find the id of the link that was clicked
	var id = element.attr("id");
	
	//Built a url to send
	var info = 'id=' + id;
	if(confirm("<?php echo $lang['delete_file']; ?>"))
	{
		var parent = $(this).parent().parent();
		$.ajax({
			type: "GET",
			url: "template/delete/deletefile.php",
			data: info,
			success: function()
			{
				parent.fadeOut('slow', function() {$(this).remove();});
			}
		});
		
		
	}
	return false;
	
});
						
					});
				</script> 
				<script type="text/javascript">
					$(function() {
						
						
						$(".btn-milestone").click(function(){
							
	//Save the link in a variable called element
	var element = $(this);
	
	//Find the id of the link that was clicked
	var id = element.attr("id");
	
	//Built a url to send
	var info = 'id=' + id;
	if(confirm("<?php echo $lang['delete_milestone']; ?>"))
	{
		var parent = $(this).parent().parent();
		$.ajax({
			type: "GET",
			url: "template/delete/deletemilestone.php",
			data: info,
			success: function()
			{
				parent.fadeOut('slow', function() {$(this).remove();});
			}
		});
		
		
	}
	return false;
	
});
						
					});
				</script>    	  
				<script type="text/javascript">
					$(function() {
						$(".btn-bug").click(function(){
							
	//Save the link in a variable called element
	var element = $(this);
	
	//Find the id of the link that was clicked
	var id = element.attr("id");
	
	//Built a url to send
	var info = 'id=' + id;
	if(confirm("<?php echo $lang['delete_bug']; ?>"))
	{
		var parent = $(this).parent().parent();
		$.ajax({
			type: "GET",
			url: "template/delete/deletebug.php",
			data: info,
			success: function()
			{
				parent.fadeOut('slow', function() {$(this).remove();});
			}
		});
		
		
	}
	return false;
	
});
						
					});
				</script> 
				<script type="text/javascript">
					function viewMilestone(id) {
			// Add User ID to the hidden field for furture usage
		 // $("#hidden_user_id").val(id);
			//$("#update_taskid").val(id);
			$.post("template/requests/readmilestonedetails.php", {
				id: id
			},
			function (data, status) {
							// PARSE json data
							var milestone = JSON.parse(data);
							// Assing existing values to the modal popup fields
							$(".name").html(milestone.name);
							$(".description").html(milestone.description);
							$(".start_date").html(milestone.start_date);
							$(".end_date").html(milestone.end_date);
							$(".p").html("$" + milestone.budget);
							
						}
						);
			// Open modal popup
			$("#viewmilestone").modal("show");
		}   
		function viewTask(id) {
			// Add User ID to the hidden field for furture usage
		 // $("#hidden_user_id").val(id);
			//$("#update_taskid").val(id);
			$.post("template/requests/readtaskdetails.php", {
				id: id
			},
			function (data, status) {
							// PARSE json data
							var task = JSON.parse(data);
							// Assing existing values to the modal popup fields
							$(".name").html(task.name);
							$(".description").html(task.description);
							$(".start_date").html(task.start_date);
							$(".end_date").html(task.end_date);
							$(".p").html(task.progress + "%");
							
						}
						);
			// Open modal popup
			$("#viewtask").modal("show");
		}	
		function viewLink(id) {
			// Add User ID to the hidden field for furture usage
		 // $("#hidden_user_id").val(id);
			//$("#update_taskid").val(id);
			$.post("template/requests/readlinkdetails.php", {
				id: id
			},
			function (data, status) {
							// PARSE json data
							var link = JSON.parse(data);
							// Assing existing values to the modal popup fields
							$(".title").html(link.title);
							$(".description").html(link.description);
							$(".link").html(link.url);
							
						}
						);
			// Open modal popup
			$("#viewlink").modal("show");
		}	
	</script> 
</body>
</html>
