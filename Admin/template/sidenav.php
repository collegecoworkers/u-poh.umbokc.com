<?
$admin = new Admin();

$basename = basename($_SERVER["REQUEST_URI"], ".php");
$editname = basename($_SERVER["REQUEST_URI"]);
?>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">

		<!-- Sidebar user panel (optional) -->
		<div class="user-panel">
			<div class="pull-left image">
				<img src="<?= escape($admin->data()->imagelocation); ?>" class="img-circle" alt="User Image" />
			</div>
			<div class="pull-left info">
				<p><?= escape($admin->data()->name); ?></p>
				<!-- Status -->
				<a href="profile.php?a=profile"><i class="fa fa-circle text-success"></i> <?= $lang['online']; ?></a>
			</div>
		</div>


		<!-- Sidebar Menu -->
		<ul class="sidebar-menu">
			<li class="header"><?= $lang['sidenav_header_1']; ?></li>
			<!-- Optionally, you can add icons to the links -->
			<li class="<?= $active = ($basename == 'dashboard') ? ' active' : ''; ?>">
				<a href="dashboard.php"><i class='fa fa-dashboard'></i> <span><?= $lang['Админ панель']; ?></span></a>
			</li>
			<li class="treeview<?= $active = ($basename == 'freelancerlist') ? ' active' : ''; echo $active = ($basename == 'addfreelancer') ? ' active' : ''; echo $active = ($editname == 'editfreelancer.php?id='. Input::get('id').'') ? ' active' : ''; ?>">
				<a href="#"><i class='fa fa-users'></i> <span><?= $lang['freelancers']; ?></span> <i class="fa fa-angle-left pull-right"></i></a>
				<ul class="treeview-menu">
					<li><a href="freelancerlist.php"><?= $lang['list']; ?></a></li>
					<li><a href="addfreelancer.php"><?= $lang['add']; ?></a></li>
				</ul>
			</li>
			<li class="treeview<?= $active = ($basename == 'clientlist') ? ' active' : ''; echo $active = ($basename == 'addclient') ? ' active' : ''; echo $active = ($editname == 'editclient.php?a='. Input::get('a').'&id='. Input::get('id').'') ? ' active' : ''; ?>">
				<a href="#"><i class='fa fa-user-md'></i> <span><?= $lang['Клиенты']; ?></span> <i class="fa fa-angle-left pull-right"></i></a>
				<ul class="treeview-menu">
					<li><a href="clientlist.php"><?= $lang['list']; ?></a></li>
					<li><a href="addclient.php"><?= $lang['add']; ?></a></li>
				</ul>
			</li>
			<li class="treeview<?= $active = ($basename == 'categorylist') ? ' active' : ''; echo $active = ($basename == 'addcategory') ? ' active' : ''; echo $active = ($editname == 'editcategory.php?id='. Input::get('id').'') ? ' active' : ''; echo $active = ($basename == 'changecategory') ? ' active' : ''; ?>">
				<a href="#"><i class='fa fa-align-left'></i> <span><?= $lang['Категории']; ?></span> <i class="fa fa-angle-left pull-right"></i></a>
				<ul class="treeview-menu">
					<li><a href="categorylist.php"><?= $lang['list']; ?></a></li>
					<li><a href="addcategory.php"><?= $lang['add']; ?></a></li>
				</ul>
			</li>
			<li class="header"><?= $lang['sidenav_header_2']; ?></li>
			<li class="<?= $active = ($basename == 'joblist') ? ' active' : ''; echo $active = ($editname == 'editjob.php?id='. Input::get('id').'') ? ' active' : ''; ?>">
				<a href="joblist.php"><i class='fa fa-align-left'></i> <span><?= $lang['Работы']; ?></span></a>
			</li>
			<li class="treeview<?php 
			echo $active = ($basename == 'proposallist') ? ' active' : ''; echo $active = ($editname == 'proposallist.php?id='. Input::get('id').'') ? ' active' : ''; echo $active = ($editname == 'viewproposal.php?id='. Input::get('id').'') ? ' active' : ''; ?>">
			<a href="proposallist.php"><i class='fa fa-files-o'></i> <span><?= $lang['proposals']; ?></span></a>
		</li> 
		<li class="<?= $active = ($basename == 'jobassigned') ? ' active' : ''; echo $active = ($editname == 'jobboard.php?a='. Input::get('a').'&id='. Input::get('id').'') ? ' active' : ''; echo $active = ($editname == 'jobboard.php?a='. Input::get('a').'&bug='. Input::get('bug').'&id='. Input::get('id').'') ? ' active' : '';?>">
			<a href="jobassigned.php"><i class='fa fa-address-card'></i> <span><?= $lang['Назначенная работа']; ?></span></a>
		</li>

<!--
		<li class="header"><?= $lang['payments']; ?></li> 
		<li class="<?= $active = ($basename == 'paymentlist') ? ' active' : '';?>">
			<a href="paymentlist.php"><i class='fa fa-list'></i> <span><?= $lang['Платежи клиентов']; ?></span></a>
		</li>
		<li class="<?= $active = ($basename == 'pay') ? ' active' : ''; echo $active = ($editname == 'pay.php?id='. Input::get('id').'') ? ' active' : '';?>">
			<a href="pay.php"><i class='fa fa-list'></i> <span><?= $lang['Оплата фрилансеров']; ?></span></a>
		</li>
	-->
	<li class="header"><?= $lang['Настройки']; ?></li>
	<li class="treeview<?php 
	echo $active = ($basename == 'home') ? ' active' : ''; echo $active = ($editname == 'settings.php?a='. Input::get('a').'') ? ' active' : ''; ?>">
	<a href="settings.php?a=site"><i class='fa fa-cogs'></i> <span><?= $lang['Настройки сайта']; ?></span></a>
</li>      
</ul><!-- /.sidebar-menu -->
</section>
<!-- /.sidebar -->
</aside>