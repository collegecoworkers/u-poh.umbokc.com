<?php
require_once 'core/frontinit.php';
?>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en"> 
<!--<![endif]-->

<!-- Include header.php. Contains header content. -->
<?php include ('includes/template/header.php'); ?> 

<body class="greybg">
	
	<!-- Include navigation.php. Contains navigation content. -->
	<?php include ('includes/template/navigation.php'); ?> 	 
	
		 <!-- ==============================================
	 Header
	 =============================================== -->	 
	 <header class="header-jobs" style="

	 background: linear-gradient(
	 	rgba(34,34,34,0.7), 
	 	rgba(34,34,34,0.7)
	 	), url('<?php echo $services_header_img; ?>') no-repeat center center fixed;
	 background-size: cover;
	 background-position: center center;
	 -webkit-background-size: cover;
	 -moz-background-size: cover;
	 -o-background-size: cover;
	 color: #fff;
	 height: 45vh;
	 width: 100%;
	 ">
	</header><!-- /header -->
	
		 <!-- ==============================================
	 Jobs Section
	 =============================================== -->
	 <section class="jobslist">
	 	<div class="container-fluid"  style="background-color: #fff; margin-top: -4em; padding-bottom: 2em;">
	 		<div class="row-fluid">
	 				<div class="col-lg-12 white">	
	 					<form action="searchpage.php" method="get" class="list-search revealOnScroll" data-animation="fadeInDown" data-timeout="200">
	 						<button><i class="fa fa-search"></i></button>
	 						<input type="text" class="form-control" name="searchterm" placeholder="<?php echo $lang['job']; ?> <?php echo $lang['title']; ?>, <?php echo $lang['keywords']; ?> <?php echo $lang['or']; ?> <?php echo $lang['company']; ?> <?php echo $lang['name']; ?>"
	 						value="<?= @$_GET['searchterm'] ?>"/>
	 						<div class="clearfix"></div>
	 					</form>

	 					<?php		

	 					$searchterm = Input::get('searchterm');
	 					$page = (int) (!isset($_GET["page"]) ? 1 : $_GET["page"]);
	 					$limit = 100;
	 					$startpoint = ($page * $limit) - $limit;	  

	 					$query = DB::getInstance()->get("profile", "*", ["OR" => [
	 						"about[~]" => $searchterm,
	 						"education[~]" => $searchterm,
	 						// "work[~]" => $searchterm,
	 						"awards[~]" => $searchterm,
	 					]]);
	 					$total = $query->count();
	 					if($query->count()) {

	 						$serviceList = '';
	 						$x = 1;

	 						foreach($query->results() as $row) {
	 							$serviceList = '';

	 							$q1 = DB::getInstance()->get("freelancer", "*", ["freelancerid" => $row->userid]);
	 							if ($q1->count()) {
	 								foreach ($q1->results() as $r1) {
	 									$name1 = $r1->name;	
	 									$username1 = $r1->username;	
	 									$imagelocation = $r1->imagelocation;	
	 								}
	 							}	

			 					$rate = $row->rate;
	 							$blurb = truncateHtml($row->about, 400);			  

	 							$admin = new Admin();
	 							$client = new Client();
	 							$freelancer = new Freelancer(); 

	 							if ($admin->isLoggedIn()) { 
	 							} elseif($freelancer->isLoggedIn()) {

	 							} elseif($client->isLoggedIn()) {
	 								$sen .='	 
	 								<a href="Client/invite.php?id='. escape($row->userid) .'" class="kafe-btn kafe-btn-mint-small">
	 								<i class="fa fa-align-left"></i>' . $lang['Предложить работу'] .'</a>
	 								';
	 							} else {
	 								$sen .='	 
	 								<a href="Client/" class="kafe-btn kafe-btn-mint-small">
	 								<i class="fa fa-align-left"></i>' . $lang['Предложить работу'] .'</a>
	 								';
	 							}

	 							$count_job = DB::getInstance()->get("job", "*", ["freelancerid" => $row->userid])->count();

	 							echo $serviceList .= '

	 							<div class="job">	

	 							<div class="row top-sec">
	 							<div class="col-lg-12">
	 							<div class="col-lg-2 col-xs-12">
	 							<a href="freelancer.php?a=overview&id='. escape($row->userid) .'">
	 							<img class="img-responsive" src="Freelancer/'. escape($imagelocation) .'" alt="">
	 							</a>
	 							</div><!-- /.col-lg-2 -->
	 							<div class="col-lg-10 col-xs-12"> 
	 							<h4><a href="freelancer.php?a=services&id='. escape($row->userid) .'&sid='. escape($row->serviceid) .'">'. escape($row->title) .'</a></h4>
	 							<h5><a href="freelancer.php?a=overview&id='. escape($row->userid) .'">'. escape($name1) .'</a></h5>
	 							</div><!-- /.col-lg-10 -->

	 							</div><!-- /.col-lg-12 -->
	 							</div><!-- /.row -->

	 							<div class="row mid-sec">			 
	 							<div class="col-lg-12">			 
	 							<div class="col-lg-12">
	 							<hr class="small-hr">
	 							'. $blurb .'
	 							</div><!-- /.col-lg-12 -->
	 							</div><!-- /.col-lg-12 -->
	 							</div><!-- /.row -->

	 							<div class="row bottom-sec">
	 							<div class="col-lg-12">

	 							<div class="col-lg-12">
	 							<hr class="small-hr">
	 							</div> 

	 							<div class="col-lg-3">
	 							<h5>' . $lang['Ставка'] . '</h5>
	 							<p>'.$currency_symbol.' '.($rate).'</p>
	 							</div>
	 							<div class="col-lg-3">
	 							<h5> ' . $lang['Выполненно работ'] . ' </h5>
	 							<p>'. escape($count_job) .'</p>
	 							</div>
	 							<div class="col-lg-4">
	 							'.$sen.'
	 							</div>

	 							</div><!-- /.col-lg-12 -->
	 							</div><!-- /.row -->

	 							</div><!-- /.job -->

	 							';

	 							unset($serviceList);	 
	 							unset($sen);	
	 							$x++;		 
	 						}
	 					}else {
	 						echo $serviceList = '<p>'.$lang['no_content_found'].'</p>';
	 					}

		//print
	 					echo Pagination($total,$limit,$page,'searchpage.php?searchterm='.$searchterm.'&');
	 					?>


	 				</div><!-- /.col-lg-8 -->
	 			</div><!-- /.row -->
	 		</div><!-- /.container-fluid -->
	 	</section><!-- /section -->  	 

	 	<!-- Include footer.php. Contains footer content. -->	
	 	<?php include 'includes/template/footer.php'; ?>	

		 <!-- ==============================================
	 Scripts
	 =============================================== -->
	 
	 <!-- jQuery 2.1.4 -->
	 <script src="assets/js/jQuery-2.1.4.min.js"></script>
	 <!-- Bootstrap 3.3.6 JS -->
	 <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
	 <!-- Waypoints JS -->
	 <script src="assets/js/waypoints.min.js" type="text/javascript"></script>
	 <!-- Kafe JS -->
	 <script src="assets/js/kafe.js" type="text/javascript"></script>

	</body>
	</html>
