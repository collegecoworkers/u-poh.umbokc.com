<?php

require_once 'core/frontinit.php';	

//Get Client's Data
$clientid = Input::get('id');
$query = DB::getInstance()->get("client", "*", ["clientid" => $clientid, "LIMIT" => 1]);
if ($query->count() === 1) {
	foreach($query->results() as $row) {
		$name = $row->name;
		$username = $row->username;
		$client_imagelocation = $row->imagelocation;
		$client_bgimage = $row->bgimage;
	}
} else {
	Redirect::to('jobs.php');
}

$query = DB::getInstance()->get("profile", "*", ["userid" => $clientid, "LIMIT" => 1]);
if ($query->count()) {
	foreach($query->results() as $row) {
		$nid = $row->id;
		$about = $row->about;
	}			
}

?>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en"> 
<!--<![endif]-->

<!-- Include header.php. Contains header content. -->
<?php include ('includes/template/header.php'); ?> 

<body class="greybg">
	
	<!-- Include navigation.php. Contains navigation content. -->
	<?php include ('includes/template/navigation.php'); ?> 	 

		 <!-- ==============================================
	 Header
	 =============================================== -->	 
	 <header class="header-freelancer" style="

	 background: linear-gradient(
	 	rgba(34,34,34,0.7), 
	 	rgba(34,34,34,0.7)
	 	), url('Client/<?php echo $client_bgimage; ?>') no-repeat center center fixed;
	 background-size: cover;
	 background-position: center center;
	 -webkit-background-size: cover;
	 -moz-background-size: cover;
	 -o-background-size: cover;
	 color: #fff;
	 height: 85vh;
	 width: 100%;

	 display: flex;
	 flex-direction: column;
	 justify-content: center;
	 align-items: center;
	 text-align: center;">
	 <div class="container">
	 	<div class="content">
	 		<div class="row">
	 			<div class="col-lg-12">
	 				<img src="Client/<?php echo $client_imagelocation; ?>" class="img-thumbnail img-responsive revealOnScroll" data-animation="fadeInDown" data-timeout="200" alt="">
	 				<h1 class="revealOnScroll" data-animation="bounceIn" data-timeout="200"> <?php echo $name; ?></h1>
	 				<a class="kafe-btn kafe-btn-mint-small"><i class="fa fa-user-md"></i> <?php echo $lang['client']; ?></a>
	 			</div><!-- /.col-lg-12 -->
	 		</div><!-- /.row -->
	 	</div><!-- /.content -->
	 </div><!-- /.container -->
	</header><!-- /header -->

		<!-- ==============================================
		Overview Section
		=============================================== -->
		
		<section class="overview" id="overview">
			<div class="container">
				<div class="row">

					<div id="sidebar" class="col-lg-4">
						<div class="list">
							<div class="list-group">

								<a class="list-group-item active cat-list">
									<em class="fa fa-fw fa-user-md"></em>&nbsp;&nbsp;&nbsp;<?php echo $lang['client']; ?>
								</a>

							</div><!-- ./.list-group -->
						</div><!-- ./.list --> 

						<?php $overview = (Input::get('a') == 'overview') ? ' active' : ''; ?>
						<?php $jobs = (Input::get('a') == 'jobs') ? ' active' : ''; ?>
						<div class="list">
							<div class="list-group">

								<a href="client.php?a=overview&id=<?php echo $clientid; ?>" class="list-group-item <?php echo $overview; ?> cat-list">
									<em class="fa fa-fw fa-align-justify"></em>&nbsp;&nbsp;&nbsp;<?php echo $lang['Обо мне']; ?>
								</a>
								<a href="client.php?a=jobs&id=<?php echo $clientid; ?>" class="list-group-item <?php echo $jobs; ?> cat-list">
									<em class="fa fa-fw fa-align-justify"></em>&nbsp;&nbsp;&nbsp;<?php echo $lang['Работы']; ?>
								</a>
							</div><!-- ./.list-group -->
						</div><!-- ./.list --> 
					</div><!-- ./.col-lg-4 -->


					<?php if (Input::get('a') == 'overview') : ?>		 
						<div class="col-lg-8 white-2">
							<div class="about">
								<h3><?php echo $lang['Обо мне']; ?></h3>
								<div class="col-lg-12 top-sec">
									<?php echo $about; ?>

									<?php
									foreach ($arr as $key => $value) {
										echo '<label class="label label-success">'. $value .'</label> &nbsp;'; 
									}
									?>		   
								</div><!-- /.col-lg-12 --> 	

								<div class="row bottom-sec">

									<div class="col-lg-12">

										<div class="col-lg-12">
											<hr class="small-hr">
										</div><!-- /.col-lg-12 --> 

										<div class="col-lg-3">
											<h5><?php echo $lang['Кол-во работ']; ?></h5>
											<p>
												<?php	
												$query = DB::getInstance()->get("job", "*", ["AND" => ['opened' => 1, "clientid" => $clientid, "invite" => 0]]);
												echo $query->count();
												?>	
											</p>
										</div><!-- /.col-lg-2 -->

									</div><!-- /.col-lg-12 -->

								</div><!-- /.col-lg-12 -->
							</div><!-- /.about -->



						</div><!-- /.col-lg-8 -->

					<?php elseif (Input::get('a') == 'jobs') : ?>		 
						<div class="col-lg-8 white-2 jobslist">
							<?php		


							$page = (int) (!isset($_GET["page"]) ? 1 : $_GET["page"]);
							$limit = $job_limit;
							$startpoint = ($page * $limit) - $limit;	  

							$q1 = DB::getInstance()->get("job", "*", ['AND' => ['opened' => 1, "clientid" => $clientid]]);
							$total = $q1->count();

							$query = DB::getInstance()->get("job", "*", ["ORDER" => "date_added DESC", "LIMIT" => [$startpoint, $limit],
								"AND" => [
									'opened' => 1, 
									"clientid" => $clientid,
								]]);
							if($query->count()) {

								$jobList = '';
								$x = 1;	


								foreach($query->results() as $row) {
									$jobList = '';

									$q1 = DB::getInstance()->get("client", "*", ["clientid" => $clientid]);
									if ($q1->count()) {
										foreach ($q1->results() as $r1) {
											$name1 = $r1->name;	
											$username1 = $r1->username;	
											$imagelocation = $r1->imagelocation;	
										}
									}	

									$q2 = DB::getInstance()->get("proposal", "*", ["jobid" => $row->jobid]);
									if ($q2->count() === 0) {
										$job_proposals = 0;	
									} else {
										$job_proposals = $q2->count();
									}			

									$blurb = truncateHtml($row->description, 400);		
									if ($row->accepted === '1' ) {
										$sen .='
										<p>' . $lang['assigned'] . '</p>
										';	
									} else {
										$sen .='
										<p>' . $lang['waiting'] . ' ' . $lang['to'] . ' ' . $lang['be'] . ' ' . $lang['assigned'] . '</p>
										';	
									}


									echo $jobList .= '
									<div class="job">	

									<div class="row top-sec">
									<div class="col-lg-12">
									<div class="col-lg-2 col-xs-12">
									<a href="freelancer.html">
									<img class="img-responsive" src="Client/'. escape($imagelocation) .'" alt="">
									</a>
									</div><!-- /.col-lg-2 -->
									<div class="col-lg-10 col-xs-12"> 
									<h4><a href="jobpost.php?title='. escape($row->slug) .'">'. escape($row->title) .'</a></h4>
									<h5><a href="client.php?a=overview&id='. escape($row->clientid) .'" 
									style="text-decoration: none !important; color: #05CB95 !important;">
									'. escape($name1) .'</a> <small>@'. escape($username1) .'</small></h5>
									</div><!-- /.col-lg-10 -->

									</div><!-- /.col-lg-12 -->
									</div><!-- /.row -->

									<div class="row mid-sec">			 
									<div class="col-lg-12">			 
									<div class="col-lg-12">
									<hr class="small-hr">
									'. $blurb .'
									</div><!-- /.col-lg-12 -->
									</div><!-- /.col-lg-12 -->
									</div><!-- /.row -->

									<div class="row bottom-sec">
									<div class="col-lg-12">

									<div class="col-lg-12">
									<hr class="small-hr">
									</div> 

									<div class="col-lg-2">
									<h5>' . $lang['Опубликованно'] . ' </h5>
									<p>'. ago(strtotime($row->date_added)) .'</p>
									</div>
									<div class="col-lg-2">
									<h5> ' . $lang['Бюджет'] . '</h5>
									<p>$'. escape($row->budget) .'</p>
									</div>
									<div class="col-lg-2">
									<h5>' . $lang['Заявки'] . '</h5>
									<p>'. escape($job_proposals) .'</p>
									</div>

									</div><!-- /.col-lg-12 -->
									</div><!-- /.row -->

									</div><!-- /.job -->
									';

									unset($jobList); 
									unset($sen);		
									$x++;		 
								}
							}else {
								echo $jobList = '<p>'.$lang['no_content_found'].'</p>';
							}

							echo Pagination($total,$limit,$page,'?a=jobs&id='.$clientid.'&');
							?>

						</div><!-- /.col-lg-8 -->

					<?php endif; ?>         
				</div><!-- /.row -->

			</div><!-- /.container --> 
		</section><!-- End section-->	 

		<!-- Include footer.php. Contains footer content. -->	
		<?php include 'includes/template/footer.php'; ?>	

		 <!-- ==============================================
	 Scripts
	 =============================================== -->
	 
	 <!-- jQuery 2.1.4 -->
	 <script src="assets/js/jQuery-2.1.4.min.js"></script>
	 <!-- Bootstrap 3.3.6 JS -->
	 <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	 <script src="assets/js/jquery.knob.js"></script>
	 <script src="assets/js/knob.js"></script>
	 <!-- Waypoints JS -->
	 <script src="assets/js/waypoints.min.js" type="text/javascript"></script>
	 <!-- Kafe JS -->
	 <script src="assets/js/kafe.js" type="text/javascript"></script>  

	</body>
	</html>
