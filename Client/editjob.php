<?php

require_once '../core/init.php';

//Start new Client object
$client = new Client();

//Check if Client is logged in
if (!$client->isLoggedIn()) {
	Redirect::to('../index.php');	
}



//Edit Category Data
if (Input::exists()) {
	if(Token::check(Input::get('token'))){

		$errorHandler = new ErrorHandler;
		
		$validator = new Validator($errorHandler);
		
		$validation = $validator->check($_POST, [
			'title' => [
				'required' => true,
				'minlength' => 2,
				'maxlength' => 200
			],
			'category' => [
				'required' => true
			],
			'budget' => [
				'required' => true,
				'digit' => true,
				'minlength' => 1,
				'maxlength' => 200
			],
			'start_date' => [
				'required' => true
			],
			'end_date' => [
				'required' => true
			],
			'description' => [
				'required' => true
			]
		]);
		
		if (!$validation->fails()) {
			
		//Update Job
			$slug = seoUrl(Input::get('title'));	
			$jobUpdate = DB::getInstance()->update('job',[
				'description' => Input::get('description'),
				'catid' => Input::get('category'),
				'title' => Input::get('title'),
				'slug' => $slug,
				'budget' => Input::get('budget'),
				'start_date' => Input::get('start_date'),
				'end_date' => Input::get('end_date'),
			],[
				'jobid' => $jobid
			]);
			
			if (count($jobUpdate) > 0) {
				$updatedError = true;
			} else {
				$hasError = true;
			}
			
			
		} else {
			$error = '';
			foreach ($validation->errors()->all() as $err) {
				$str = implode(" ",$err);
				$error .= '
				<div class="alert alert-danger fade in">
				<a href="#" class="close" data-dismiss="alert">&times;</a>
				<strong>Error!</strong> '.$str.'
				</div>
				';
			}
		}

	}
}

//Getting Job Data
$jobid = Input::get('id');
$query = DB::getInstance()->get("job", "*", ["jobid" => $jobid, "LIMIT" => 1]);
if ($query->count() === 1) {
	foreach($query->results() as $row) {
		$jobid = $row->jobid;
		$catid = $row->catid;
		$job_title = $row->title;
		$job_type = $row->job_type;
		$job_budget = $row->budget;
		$job_description = $row->description;
		$job_start_date = $row->start_date;
		$job_end_date = $row->end_date;
	}
} else {
	Redirect::to('joblist.php');
}	
?>
<!DOCTYPE html>
<html lang="en-US" class="no-js">

<!-- Include header.php. Contains header content. -->
<?php include ('template/header.php'); ?> 
<!-- Theme style -->
<link href="../assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<!-- Bootstrap Select CSS-->
<link  href="../assets/css/bootstrap-select.css" rel="stylesheet" type="text/css" />

<body class="skin-green sidebar-mini">
	
		 <!-- ==============================================
		 Wrapper Section
		 =============================================== -->
		 <div class="wrapper">

		 	<!-- Include navigation.php. Contains navigation content. -->
		 	<?php include ('template/navigation.php'); ?> 
		 	<!-- Include sidenav.php. Contains sidebar content. -->
		 	<?php include ('template/sidenav.php'); ?> 

		 	<!-- Content Wrapper. Contains page content -->
		 	<div class="content-wrapper">
		 		<!-- Content Header (Page header) -->
		 		<section class="content-header">
		 			<h1><?= $lang['Редактировать работу']; ?><small><?= $lang['section']; ?></small></h1>
		 			<ol class="breadcrumb">
		 				<li><a href="index.php"><i class="fa fa-dashboard"></i> <?= $lang['home']; ?></a></li>
		 				<li class="active"><?= $lang['редактировать работу']; ?></li>
		 			</ol>
		 		</section>

		 		<!-- Main content -->
		 		<section class="content">	 	
		 			<!-- Include currency.php. Contains header content. -->
		 			<?php include ('template/currency.php'); ?>  
		 			<div class="row">	

		 				<div class="col-lg-12">
		 					<?php if(isset($hasError)) { //If errors are found ?>
		 					<div class="alert alert-danger fade in">
		 						<a href="#" class="close" data-dismiss="alert">&times;</a>
		 						<strong><?= $lang['hasError']; ?></strong> <?= $lang['has_Error']; ?>
		 					</div>
		 					<?php } ?>

		 					<?php if(isset($updatedError) && $updatedError == true) { //If email is sent ?>
		 					<div class="alert alert-success fade in">
		 						<a href="#" class="close" data-dismiss="alert">&times;</a>
		 						<strong><?= $lang['noError']; ?></strong> <?= $lang['updated_success']; ?></strong>
		 					</div>
		 					<?php } ?>

		 					<?php if (isset($error)) {
		 						echo $error;
		 					} ?>  
		 				</div>	


		 				<div class="col-lg-12">
		 					<!-- Input addon -->
		 					<div class="box box-info">
		 						<div class="box-header">
		 							<h3 class="box-title"><?= $lang['Редактировать']; ?></h3>
		 						</div>
		 						<div class="box-body">
		 							<form role="form" method="post" id="editform"> 

		 								<div class="form-group">	
		 									<label><?= $lang['Название']; ?></label>
		 									<div class="input-group">
		 										<span class="input-group-addon"><i class="fa fa-info"></i></span>
		 										<input type="text" name="title" class="form-control" value="<?php
		 										if (isset($_POST['details'])) {
		 											echo escape(Input::get('title')); 
		 										} else {
		 											echo escape($job_title); 
		 										}
		 										?>"/>
		 									</div>
		 								</div>

		 								<div class="form-group">	
		 									<label><?= $lang['Категория']; ?></label>
		 									<div class="input-group">
		 										<span class="input-group-addon"><i class="fa fa-pencil-square"></i></span>
		 										<select name="category" type="text" class="form-control">
		 											<?php
		 											$query = DB::getInstance()->get("category", "*", ["AND" => ["active" => 1, "delete_remove" => 0]]);
		 											if ($query->count()) {
		 												$categoryname = '';
		 												$x = 1;
		 												foreach ($query->results() as $row) {

		 													if (isset($_POST['details'])) {
		 														$selected = (Input::get('category') === $catid) ? ' selected="selected"' : '';
		 													} else {
		 														$selected = ($row->catid === $catid) ? ' selected="selected"' : '';
		 													}

		 													echo $categoryname .= '<option value = "' . $row->catid . '" '.$selected.'>' . $row->name . '</option>';
		 													unset($categoryname); 
		 													$x++;
		 												}
		 											}
		 											?>	
		 										</select>
		 									</div>
		 								</div>

		 								<div class="form-group">	
		 									<label><?= $lang['budget']; ?></label>
		 									<div class="input-group">
		 										<span class="input-group-addon"><i class="fa fa-money"></i></span>
		 										<input type="text" name="budget" class="form-control" value="<?php
		 										if (isset($_POST['details'])) {
		 											echo escape(Input::get('budget')); 
		 										} else {
		 											echo escape($job_budget); 
		 										}
		 										?>"/>
		 									</div>
		 								</div>

		 								<div class="form-group">
		 									<label for="dtp_input1"><?= $lang['start']; ?> <?= $lang['date']; ?></label>
		 									<div class="input-group date form_datetime_start" data-date-format="dd MM yyyy" data-link-field="dtp_input1">
		 										<input name="start_date" class="form-control" type="text" value="<?php
		 										if (isset($_POST['details'])) {
		 											echo escape(Input::get('start_date')); 
		 										} else {
		 											echo escape($job_start_date); 
		 										}
		 										?>" readonly>
		 										<span class="input-group-addon"><i class="glyphicon glyphicon-remove"></i></span>
		 										<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
		 									</div>
		 									<input type="hidden" id="dtp_input1" value="" /><br/>
		 									<input name="mirror_field_start" type="hidden" id="mirror_field_start" class="form-control" readonly />
		 									<input name="mirror_field_start_date" type="hidden" id="mirror_field_start_date" class="form-control" readonly />
		 								</div>       

		 								<div class="form-group">
		 									<label for="dtp_input1"><?= $lang['estimated']; ?> <?= $lang['end']; ?> <?= $lang['date']; ?></label>
		 									<div class="input-group date form_datetime_start" data-date-format="dd MM yyyy" data-link-field="dtp_input1">
		 										<input name="end_date" class="form-control" type="text" value="<?php
		 										if (isset($_POST['details'])) {
		 											echo escape(Input::get('end_date')); 
		 										} else {
		 											echo escape($job_end_date); 
		 										}
		 										?>" readonly>
		 										<span class="input-group-addon"><i class="glyphicon glyphicon-remove"></i></span>
		 										<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
		 									</div>
		 									<input type="hidden" id="dtp_input1" value="" /><br/>
		 									<input name="mirror_field_start" type="hidden" id="mirror_field_start" class="form-control" readonly />
		 									<input name="mirror_field_start_date" type="hidden" id="mirror_field_start_date" class="form-control" readonly />
		 								</div> 

		 								<div class="form-group">	
		 									<label><?= $lang['description']; ?></label>
		 									<textarea type="text" id="summernote" name="description" class="form-control"><?php
		 									if (isset($_POST['details'])) {
		 										echo escape(Input::get('description')); 
		 									} else {
		 										echo escape($job_description); 
		 									}
		 									?></textarea>
		 								</div>

		 								<div class="box-footer">
		 									<input type="hidden" name="token" value="<?= Token::generate(); ?>" />
		 									<button type="submit" name="details" class="btn btn-primary full-width"><?= $lang['submit']; ?></button>
		 								</div>
		 							</form> 
		 						</div><!-- /.box-body -->
		 					</div><!-- /.box -->



		 				</div><!-- /.col -->



		 			</div><!-- /.row -->		  		  
		 		</section><!-- /.content -->
		 	</div><!-- /.content-wrapper -->

		 	<!-- Include footer.php. Contains footer content. -->	
		 	<?php include 'template/footer.php'; ?>	

		 </div><!-- /.wrapper -->   

		 
	<!-- ==============================================
	 Scripts
	 =============================================== -->
	 
	 <!-- jQuery 2.1.4 -->
	 <script src="../assets/js/jQuery-2.1.4.min.js"></script>
	 <!-- Bootstrap 3.3.6 JS -->
	 <script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
	 <!-- AdminLTE App -->
	 <script src="../assets/js/app.min.js" type="text/javascript"></script>
	 <!-- Datetime Picker -->
	 <script src="../assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
	 <script type="text/javascript">
	 	$('.form_datetime_start').datetimepicker({
	 		language:  'ru',
	 		showToday: false,                 
	 		useCurrent: false,
	 		weekStart: 1,
	 		todayBtn:  1,
	 		autoclose: 1,
	 		todayHighlight: 1,
	 		startView: 2,
	 		forceParse: 0,
	 		showMeridian: 1, 
	 		pickTime: false, 
	 		minView: 2,      
	 		pickerPosition: "bottom-left",
	 		linkField: "mirror_field_start",
	 		linkFormat: "hh:ii",
	 		linkFieldd: "mirror_field_start_date",
	 		linkFormatt: "dd MM yyyy"
	 	});
	 	$('.form_datetime_end').datetimepicker({
	 		language:  'ru',
	 		weekStart: 1,
	 		todayBtn:  1,
	 		autoclose: 1,
	 		todayHighlight: 1,
	 		startView: 2,
	 		forceParse: 0,
	 		showMeridian: 1, 
	 		pickTime: false, 
	 		minView: 2,      
	 		pickerPosition: "bottom-left",
	 		linkField: "mirror_field_start",
	 		linkFormat: "hh:ii",
	 		linkFieldd: "mirror_field_start_date",
	 		linkFormatt: "dd MM yyyy"
	 	});
	 </script>
	 <!-- Summernote WYSIWYG-->
	 <script src="../assets/js/summernote.min.js" type="text/javascript"></script>    
	 <script>
	 	$(document).ready(function() {
	 		$('#summernote').summernote({
			height: 300,                 // set editor height
			
			minHeight: null,             // set minimum height of editor
			maxHeight: null,             // set maximum height of editor
			
			focus: false,                 // set focus to editable area after initializing summernote
		});    
	 	});
	 </script>
	 <!-- Bootstrap Select JS-->
	 <script src="../assets/js/bootstrap-select.js"></script>
	</body>
	</html>
