<?
$client = new Client();

$basename = basename($_SERVER["REQUEST_URI"], ".php");
$editname = basename($_SERVER["REQUEST_URI"]);
?>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">

    <!-- Sidebar user panel (optional) -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?= escape($client->data()->imagelocation); ?>" class="img-circle" alt="User Image" />
      </div>
      <div class="pull-left info">
        <p><?= escape($client->data()->name); ?></p>
        <!-- Status -->
        <a href="profile.php?a=profile"><i class="fa fa-circle text-success"></i> <?= $lang['online']; ?></a>
      </div>
    </div>


    <!-- Sidebar Menu -->
    <ul class="sidebar-menu">
      <li class="header"><?= $lang['sidenav_header_2']; ?></li>
      <!-- Optionally, you can add icons to the links -->
      <li class="<?= $active = ($basename == 'index') ? ' active' : ''; ?>">
       <a href="index.php"><i class='fa fa-dashboard'></i> <span><?= $lang['dashboard']; ?></span></a>
     </li>
     <li class="treeview<?= $active = ($basename == 'joblist') ? ' active' : ''; echo $active = ($basename == 'addjob') ? ' active' : ''; echo $active = ($editname == 'editjob.php?id='. Input::get('id').'') ? ' active' : ''; ?>">
      <a href="#"><i class='fa fa-align-left'></i> <span><?= $lang['Работы']; ?></span> <i class="fa fa-angle-left pull-right"></i></a>
      <ul class="treeview-menu">
        <li><a href="joblist.php"><?= $lang['list']; ?></a></li>
        <li><a href="addjob.php"><?= $lang['add']; ?></a></li>
      </ul>
    </li> 
    <li class="treeview<?= $active = ($basename == 'jobinvite') ? ' active' : ''; echo $active = ($basename == 'addinvite') ? ' active' : ''; echo $active = ($editname == 'editinvite.php?id='. Input::get('id').'') ? ' active' : ''; echo $active = ($editname == 'invite.php?id='. Input::get('id').'') ? ' active' : ''; echo $active = ($editname == 'viewinvite.php?id='. Input::get('id').'') ? ' active' : '';?>">
      <a href="#"><i class='fa fa-filter'></i> <span><?= $lang['Приглашения']; ?></span> <i class="fa fa-angle-left pull-right"></i></a>
      <ul class="treeview-menu">
        <li><a href="jobinvite.php"><?= $lang['list']; ?></a></li>
        <li><a href="addinvite.php"><?= $lang['Пригласить']; ?></a></li>
      </ul>
    </li> 
    <li class="treeview<?php 
    echo $active = ($basename == 'proposallist') ? ' active' : ''; echo $active = ($editname == 'proposallist.php?id='. Input::get('id').'') ? ' active' : ''; echo $active = ($editname == 'editproposal.php?id='. Input::get('id').'') ? ' active' : ''; echo $active = ($editname == 'viewproposal.php?id='. Input::get('id').'') ? ' active' : ''; ?>">
    <a href="proposallist.php"><i class='fa fa-files-o'></i> <span><?= $lang['proposals']; ?></span>
      <span class="label label-info pull-right">
        <?php 	
        $q1 = DB::getInstance()->get("proposal", ["[>]job" => ["proposal.jobid" => "jobid"]], "*", ["AND" => ["job.clientid" => $client->data()->clientid]]);
        echo $q1->count();
        ?></span></a>
      </li>    
      <li class="<?= $active = ($basename == 'jobassigned') ? ' active' : ''; echo $active = ($editname == 'jobboard.php?a='. Input::get('a').'&id='. Input::get('id').'') ? ' active' : '';?>">
       <a href="jobassigned.php"><i class='fa fa-address-card'></i> <span><?= $lang['Назначенная работа']; ?></span></a>
     </li>       
     <li class="header"><?= $lang['Информация профиля']; ?></li>          
     <li class="treeview<?php 
     echo $active = ($basename == 'overview') ? ' active' : '';   ?>">
     <a href="overview.php?a=profile"><i class='fa fa-info-circle'></i> <span><?= $lang['Страница']; ?></span></a>
   </li> 
 </ul><!-- /.sidebar-menu -->
</section>
<!-- /.sidebar -->
</aside>