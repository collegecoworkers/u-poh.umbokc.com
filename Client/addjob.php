<?php
require_once '../core/init.php';

//Start new Client object
$client = new Client();

//Check if Client is logged in
if (!$client->isLoggedIn()) {
	Redirect::to('../index.php');	
}

//Add Category Function
if (Input::exists()) {
	if(Token::check(Input::get('token'))){
		
		$errorHandler = new ErrorHandler;
		
		$validator = new Validator($errorHandler);
		
		$validation = $validator->check($_POST, [
			'title' => [
				'required' => true,
				'minlength' => 2,
				'maxlength' => 200
			],
			'category' => [
				'required' => true
			],
			'budget' => [
				'required' => true,
				'digit' => true,
				'minlength' => 1,
				'maxlength' => 200
			],
			'start_date' => [
				'required' => true
			],
			'end_date' => [
				'required' => true
			],
			'description' => [
				'required' => true
			]
		]);
		
		if (!$validation->fails()) {
			
			try{
				$jobid = uniqueid();	
				$slug = seoUrl(Input::get('title'));	
				$jobInsert = DB::getInstance()->insert('job', array(
					'description' => Input::get('description'),
					'jobid' => $jobid,
					'clientid' => $client->data()->clientid,
					'catid' => Input::get('category'),
					'title' => Input::get('title'),
					'slug' => $slug,
					'budget' => Input::get('budget'),
					'start_date' => Input::get('start_date'),
					'end_date' => Input::get('end_date'),
					'invite' => 0,
					'date_added' => date('Y-m-d H:i:s')
				));	
				
				if (count($jobInsert) > 0) {
					$noError = true;
				} else {
					$hasError = true;
				}
				
				
			}catch(Exception $e){
				die($e->getMessage());	
			} 
			
		} else {
			$error = '';
			foreach ($validation->errors()->all() as $err) {
				$str = implode(" ",$err);
				$error .= '
				<div class="alert alert-danger fade in">
				<a href="#" class="close" data-dismiss="alert">&times;</a>
				<strong>Error!</strong> '.$str.'
				</div>
				';
			}
		}
		
	}	  
}

?>
<!DOCTYPE html>
<html lang="en-US" class="no-js">

<!-- Include header.php. Contains header content. -->
<?php include ('template/header.php'); ?> 
<!-- Bootstrap Datetimepicker CSS -->
<link href="../assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<!-- Bootstrap Select CSS-->
<link  href="../assets/css/bootstrap-select.css" rel="stylesheet" type="text/css" />

<body class="skin-green sidebar-mini">
	
		 <!-- ==============================================
		 Wrapper Section
		 =============================================== -->
		 <div class="wrapper">

		 	<!-- Include navigation.php. Contains navigation content. -->
		 	<?php include ('template/navigation.php'); ?> 
		 	<!-- Include sidenav.php. Contains sidebar content. -->
		 	<?php include ('template/sidenav.php'); ?> 

		 	<!-- Content Wrapper. Contains page content -->
		 	<div class="content-wrapper">
		 		<!-- Content Header (Page header) -->
		 		<section class="content-header">
		 			<h1><?= $lang['Добавить работу']; ?><small><?= $lang['section']; ?></small></h1>
		 			<ol class="breadcrumb">
		 				<li><a href="index.php"><i class="fa fa-dashboard"></i> <?= $lang['home']; ?></a></li>
		 				<li class="active"><?= $lang['добавить работу']; ?></li>
		 			</ol>
		 		</section>

		 		<!-- Main content -->
		 		<section class="content">	
		 			<!-- Include currency.php. Contains header content. -->
		 			<?php include ('template/currency.php'); ?>   	
		 			<div class="row">	

		 				<div class="col-lg-12">	
		 					<?php if(isset($hasError)) { //If errors are found ?>
		 					<div class="alert alert-danger fade in">
		 						<a href="#" class="close" data-dismiss="alert">&times;</a>
		 						<strong><?= $lang['hasError']; ?></strong> <?= $lang['has_Error']; ?>
		 					</div>
		 					<?php } ?>

		 					<?php if(isset($noError) && $noError == true) { //If email is sent ?>
		 					<div class="alert alert-success fade in">
		 						<a href="#" class="close" data-dismiss="alert">&times;</a>
		 						<strong><?= $lang['noError']; ?></strong> <?= $lang['saved_success']; ?></strong>
		 					</div>
		 					<?php } ?>

		 					<?php if (isset($error)) {
		 						echo $error;
		 					} ?>


		 				</div>

		 				<div class="col-lg-12">

		 					<!-- Input addon -->
		 					<div class="box box-info">
		 						<div class="box-header">
		 							<h3 class="box-title"><?= $lang['add']; ?> <?= $lang['работу']; ?></h3>
		 						</div>
		 						<div class="box-body">
		 							<form role="form" method="post" id="addform"> 

		 								<div class="form-group">	
		 									<label><?= $lang['Название']; ?></label>
		 									<div class="input-group">
		 										<span class="input-group-addon"><i class="fa fa-info"></i></span>
		 										<input type="text" name="title" class="form-control" placeholder="<?= $lang['Название']; ?>" value="<?= escape(Input::get('title')); ?>"/>
		 									</div>
		 								</div>

		 								<div class="form-group">	
		 									<label><?= $lang['Категория']; ?></label>
		 									<div class="input-group">
		 										<span class="input-group-addon"><i class="fa fa-pencil-square"></i></span>
		 										<select name="category" type="text" class="form-control">
		 											<?php
		 											$query = DB::getInstance()->get("category", "*", ["AND" => ["active" => 1, "delete_remove" => 0]]);
		 											if ($query->count()) {
		 												$categoryname = '';
		 												$x = 1;
		 												foreach ($query->results() as $row) {
		 													echo $categoryname .= '<option value = "' . $row->catid . '">' . $row->name . '</option>';
		 													unset($categoryname); 
		 													$x++;
		 												}
		 											}
		 											?>	
		 										</select>
		 									</div>
		 								</div>

		 								<div class="form-group">	
		 									<label><?= $lang['Бюджет']; ?></label>
		 									<div class="input-group">
		 										<span class="input-group-addon"><i class="fa fa-money"></i></span>
		 										<input type="text" name="budget" class="form-control" placeholder="<?= $lang['Бюджет']; ?>" value="<?= escape(Input::get('budget')); ?>"/>
		 									</div>
		 								</div>       

		 								<div class="form-group">
		 									<label for="dtp_input1"><?= $lang['Дата начала']; ?></label>
		 									<div class="input-group date form_datetime_start" data-date-format="dd MM yyyy" data-link-field="dtp_input1">
		 										<input name="start_date" class="form-control" type="text" value="" readonly>
		 										<span class="input-group-addon"><i class="glyphicon glyphicon-remove"></i></span>
		 										<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
		 									</div>
		 									<input type="hidden" id="dtp_input1" value="" /><br/>
		 									<input name="mirror_field_start" type="hidden" id="mirror_field_start" class="form-control" readonly />
		 									<input name="mirror_field_start_date" type="hidden" id="mirror_field_start_date" class="form-control" readonly />
		 								</div> 

		 								<div class="form-group">
		 									<label for="dtp_input1"><?= $lang['Дата конца']; ?></label>
		 									<div class="input-group date form_datetime_end" data-date-format="dd MM yyyy" data-link-field="dtp_input1">
		 										<input name="end_date" class="form-control" type="text" value="" readonly>
		 										<span class="input-group-addon"><i class="glyphicon glyphicon-remove"></i></span>
		 										<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
		 									</div>
		 									<input type="hidden" id="dtp_input1" value="" /><br/>
		 									<input name="mirror_field_start" type="hidden" id="mirror_field_start" class="form-control" readonly />
		 									<input name="mirror_field_start_date" type="hidden" id="mirror_field_start_date" class="form-control" readonly />
		 								</div> 

		 								<div class="form-group">	
		 									<label><?= $lang['description']; ?></label>
		 									<textarea type="text" id="summernote" name="description" class="form-control"></textarea>
		 								</div>

		 								<div class="box-footer">
		 									<input type="hidden" name="token" value="<?= Token::generate(); ?>" />
		 									<button type="submit" name="data" class="btn btn-primary full-width"><?= $lang['submit']; ?></button>
		 								</div>
		 							</form> 
		 						</div><!-- /.box-body -->
		 					</div><!-- /.box -->

		 				</div><!-- /.col -->



		 			</div><!-- /.row -->		  		  
		 		</section><!-- /.content -->
		 	</div><!-- /.content-wrapper -->

		 	<!-- Include footer.php. Contains footer content. -->	
		 	<?php include 'template/footer.php'; ?>	

		 </div><!-- /.wrapper -->   

		 
	<!-- ==============================================
	 Scripts
	 =============================================== -->
	 
	 <!-- jQuery 2.1.4 -->
	 <script src="../assets/js/jQuery-2.1.4.min.js"></script>
	 <!-- Bootstrap 3.3.6 JS -->
	 <script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
	 <!-- AdminLTE App -->
	 <script src="../assets/js/app.min.js" type="text/javascript"></script>
	 <!-- Datetime Picker -->
	 <script src="../assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
	 <script type="text/javascript">
	 	$('.form_datetime_start').datetimepicker({
	 		language:  'ru',
	 		weekStart: 1,
	 		todayBtn:  1,
	 		autoclose: 1,
	 		todayHighlight: 1,
	 		startView: 2,
	 		forceParse: 0,
	 		showMeridian: 1, 
	 		startDate: new Date(),
	 		pickTime: false, 
	 		minView: 2,      
	 		pickerPosition: "bottom-left",
	 		linkField: "mirror_field_start",
	 		linkFormat: "hh:ii",
	 		linkFieldd: "mirror_field_start_date",
	 		linkFormatt: "dd MM yyyy"
	 	});
	 	$('.form_datetime_end').datetimepicker({
	 		language:  'ru',
	 		weekStart: 1,
	 		todayBtn:  1,
	 		autoclose: 1,
	 		todayHighlight: 1,
	 		startView: 2,
	 		forceParse: 0,
	 		showMeridian: 1, 
	 		startDate: new Date(),
	 		pickTime: false, 
	 		minView: 2,      
	 		pickerPosition: "bottom-left",
	 		linkField: "mirror_field_start",
	 		linkFormat: "hh:ii",
	 		linkFieldd: "mirror_field_start_date",
	 		linkFormatt: "dd MM yyyy"
	 	});
	 </script>
	 <!-- Summernote WYSIWYG-->
	 <script src="../assets/js/summernote.min.js" type="text/javascript"></script>    
	 <script>
	 	$(document).ready(function() {
	 		$('#summernote').summernote({
			height: 300,                 // set editor height
			
			minHeight: null,             // set minimum height of editor
			maxHeight: null,             // set maximum height of editor
			
			focus: false,                 // set focus to editable area after initializing summernote
		});    
	 	});
	 </script>
	 <!-- Bootstrap Select JS-->
	 <script src="../assets/js/bootstrap-select.js"></script>
	</body>
	</html>
