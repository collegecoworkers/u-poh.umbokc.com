<?php

require_once 'core/frontinit.php';	

?>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en"> 
<!--<![endif]-->

<!-- Include header.php. Contains header content. -->
<?php include ('includes/template/header.php'); ?> 

<body class="greybg">
	
	<!-- Include navigation.php. Contains navigation content. -->
	<?php include ('includes/template/navigation.php'); ?> 	 
	
		 <!-- ==============================================
	 Header
	 =============================================== -->	 
	 <header class="header-jobs" style="

	 background: linear-gradient(
	 	rgba(34,34,34,0.7), 
	 	rgba(34,34,34,0.7)
	 	), url('<?php echo $services_header_img; ?>') no-repeat center center fixed;
	 background-size: cover;
	 background-position: center center;
	 -webkit-background-size: cover;
	 -moz-background-size: cover;
	 -o-background-size: cover;
	 color: #fff;
	 height: 45vh;
	 width: 100%;
	 ">
	</header><!-- /header -->
	
		 <!-- ==============================================
	 Jobs Section
	 =============================================== -->
	 <section class="jobslist">
	 	<div class="container-fluid ">
	 		<div class="row white" style="background-color: #fff;">

	 				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 white">	

	 					<?php
	 					$page = (int) (!isset($_GET["page"]) ? 1 : $_GET["page"]);
	 					$limit = $service_limit;
	 					$startpoint = ($page * $limit) - $limit;	  

	 					$q1 = DB::getInstance()->get("freelancer", "*");
	 					$total = $q1->count();
	 					$query = DB::getInstance()->get("freelancer", "*", ["ORDER" => "id DESC", "LIMIT" => [$startpoint, $limit]]);
	 					if($query->count()) {
	 						$x = 1;
	 						foreach($query->results() as $row) {
	 							$name1 = $row->name;	
	 							$username1 = $row->username;	
	 							$imagelocation = $row->imagelocation;

	 							$q2 = DB::getInstance()->get("profile", "*", ["userid" => $row->freelancerid]);
	 							if ($q2->count()) {
	 								foreach($q2->results() as $r2) {
	 									$country = $r2->country;
	 									$rate = $r2->rate;
			 							$blurb = truncateHtml($r2->about, 400);			  
	 								}
	 							}

	 							$count_job = DB::getInstance()->get("job", "*", ["freelancerid" => $row->freelancerid])->count();

	 							$admin = new Admin();
	 							$client = new Client();
	 							$freelancer = new Freelancer(); 


	 							?>
									<div class="job"> 
										<div class="row top-sec">
											<div class="col-lg-12">
												<div class="col-lg-2 col-xs-12">
													<a href="freelancer.php?a=overview&id=<?= escape($row->freelancerid) ?>">
														<img class="img-responsive" src="Freelancer/<?= escape($imagelocation) ?>" alt="">
													</a>
												</div><!-- /.col-lg-2 -->
												<div class="col-lg-10 col-xs-12"> 
													<h4><a href="freelancer.php?a=overview&id=<?= escape($row->freelancerid) ?>"><?= escape($name1) ?></a> <small>@Freelancer</small></h4>
												</div><!-- /.col-lg-10 -->
											</div><!-- /.col-lg-12 -->
										</div><!-- /.row -->
										
										<div class="row mid-sec">      
											<div class="col-lg-12">      
												<div class="col-lg-12">
													<hr class="small-hr">
													<p><?= $blurb ?><br></p>
												</div><!-- /.col-lg-12 -->
											</div><!-- /.col-lg-12 -->
										</div><!-- /.row -->
										
										<div class="row bottom-sec">
											<div class="col-lg-12">
												
												<div class="col-lg-12">
													<hr class="small-hr">
												</div> 
												
												<div class="col-lg-2">
													<h5>  Ставка </h5>
													<p><?= $currency_symbol.' '. escape($rate) ?></p>
												</div>
												<div class="col-lg-2">
													<h5>  Выполненно работ </h5>
													<p><?= $count_job ?></p>
												</div>
												
											</div><!-- /.col-lg-12 -->
										</div><!-- /.row -->
										
									</div>
								<?php 

	 							
	 							$x++;		 
	 						}
	 					}else {
	 						echo '<p>'.$lang['no_content_found'].'</p>';
	 					}

	 					echo Pagination($total,$limit,$page);
	 					?>		  

	 				</div><!-- /.col-lg-8 -->
	 			</div><!-- /.row -->
	 		</div><!-- /.container-fluid -->
	 	</section><!-- /section -->  	 

	 	<!-- Include footer.php. Contains footer content. -->	
	 	<?php include 'includes/template/footer.php'; ?>	

		 <!-- ==============================================
	 Scripts
	 =============================================== -->
	 
	 <!-- jQuery 2.1.4 -->
	 <script src="assets/js/jQuery-2.1.4.min.js"></script>
	 <!-- Bootstrap 3.3.6 JS -->
	 <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
	 <!-- Waypoints JS -->
	 <script src="assets/js/waypoints.min.js" type="text/javascript"></script>
	 <!-- Kafe JS -->
	 <script src="assets/js/kafe.js" type="text/javascript"></script>

	</body>
	</html>
