<?php
error_reporting(E_ERROR | E_PARSE);
ini_set('display_errors', 'on');
error_reporting(E_ALL);

session_start();

$GLOBALS['config'] = array(
  'mysql' => array(
    'host' => 'localhost',
    'username' => '046652434_u-poh',
    'password' => '046652434',
    'db' => 'st-umbokc_u-poh'
  ),
  'remember' => array(
    'cookie_name' => 'hash',
    'cookie_expiry' => 604800
  ),
  'session' => array(
    'session_admin' => 'admin',
    'session_client' => 'client',
    'session_name' => 'freelancer',
    'token_name' => 'token'
  )
);


spl_autoload_register(function($class){
  require_once '../../classes/' .$class. '.php';	
});


require_once '../../functions/sanitize.php';
require_once '../../functions/Functions.php';

?>