<?php

include_once 'TruncateHTML.php';

//Clean URL
function seoUrl($string) {
// 		//Lower case everything
// 	$string = strtolower($string);
// 		//Make alphanumeric (removes all other characters)
// 	$string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
// 		//Clean up multiple dashes or whitespaces
// 	$string = preg_replace("/[\s-]+/", " ", $string);
// 		//Convert whitespaces and underscore to dash
// 	$string = preg_replace("/[\s_]/", "-", $string);
// 	return $string;


	$string = strtolower($string);
	$string = mb_convert_case($string, MB_CASE_LOWER, "UTF-8");
	$string = preg_replace("/[\s-]+/", " ", $string);
	$string = preg_replace("/[\s_]/", "-", $string);
	return $string;
}


//Year
function year_now($year = 'auto', $name){
	if(intval($year) == 'auto'){
		$year = date('Y').$name;
	}
	if(intval($year) == date('Y')){
		echo intval($year).$name;
	}
	if(intval($year) < date('Y')){
		echo intval($year) . ' - ' . date('Y').$name;
	}
	if(intval($year) > date('Y')){
		echo date('Y').$name;
	}
}

//Date Difference
function datediff($timestart, $timeend) {
	
		// If not numeric then convert texts to unix timestamps
	if (!is_int($timestart)) {
		$time1 = strtotime($timestart, 0);
	}
	if (!is_int($timeend)) {
		$time2 = strtotime($timeend, 0);
	}	
	
	// Difference in seconds
	$difference = $time2 - $time1; 
	
	$numberDays = $difference/86400;  // 86400 seconds in one day
	
	// and you might want to convert to integer
	$numberDays = intval($numberDays);	
	
	// Difference in seconds
		//$ti = gmdate("H:i", $difference); 	
	
	return $numberDays;	
}

//Money As Cents
function getMoneyAsCents($value)
{
		// strip out commas
	$value = preg_replace("/\,/i","",$value);
		// strip out all but numbers, dash, and dot
	$value = preg_replace("/([^0-9\.\-])/i","",$value);
		// make sure we are dealing with a proper number now, no +.4393 or 3...304 or 76.5895,94
	if (!is_numeric($value))
	{
		return 0.00;
	}
		// convert to a float explicitly
	$value = (float)$value;
	return round($value,2)*100;
}

//Change Featured Job
function featured($jobid) {
	
	$q1 = DB::getInstance()->get("job", "*", ["jobid" => $jobid, "LIMIT" => 1]);
	if($q1->count()) {
		foreach($q1->results() as $r1) {
			$featured_date = $r1->featured_date;
		}
	}	

	$q2 = DB::getInstance()->get("payments_settings", "*", ["id" => 1, "LIMIT" => 1]);
	if($q2->count()) {
		foreach($q2->results() as $r2) {
			$jobs_pay_limit = $r2->jobs_pay_limit;
		}
	}	 

		// Check Number of days
	$timenow = date("F j, Y, g:i a");
	$timeend =  $featured_date. ' + ' .$jobs_pay_limit;
	$datediff = datediff($timenow, $timeend);     

	if ($datediff <= 0) {

		//Update Membership
		$Update = DB::getInstance()->update('job',[
			'featured' => 0
		],[
			'jobid' => $jobid
		]);		

	}

}

//Time Ago
function ago($i){
	$m = time()-$i; $o='Только что';
	$t = array('год'=>31556926,'месяц'=>2629744,'неделю'=>604800, 'день'=>86400,'час'=>3600,'минуту'=>60,'секуду'=>1);
	foreach($t as $u=>$s){
		if($s<=$m){
			$v=floor($m/$s); $o="$v $u".($v==1?'':'s').' назад'; break;
		}
	}
	return $o;
}

function getProposals($id, $jobid, $limit) {
	
	$comList = '';
	$c_per_page = $limit;
	
	
	if ($id = null) {
		$q = null;	  
	}	else {
		$q = '"id[<]" => $id';
	}		

	$q1 = DB::getInstance()->get("proposal", "*", ["AND" => [$q, "jobid" => $jobid, ]]);
		// If there are more results available than the limit, then show the Load More Comments
	$count = $q1->count();
	if($count > $c_per_page) {
		$loadmore = 1;
	} else {
		$loadmore = 0;
	}		

	$query = DB::getInstance()->get("proposal", "*", ["ORDER" => "id DESC", "AND" => [$q, "jobid" => $jobid, ], "LIMIT" => $c_per_page]);
	if($query->count()) {
		$x = 1;	
		foreach($query->results() as $row) {	

			$q3 = DB::getInstance()->get("freelancer", "*", ["freelancerid" => $row->freelancerid, "LIMIT" => 1]);
			if($q3->count()) {
				foreach($q3->results() as $r3) {
					$name_3 = $r3->name;
					$username_3 = $r3->username;
					$imagelocation_3 = $r3->imagelocation;
				}
			}		

			$q4 = DB::getInstance()->get("profile", "*", ["userid" => $row->freelancerid, "LIMIT" => 1]);
			if ($q4->count()) {
				foreach($q4->results() as $r4) {
					$country = $r4->country;
				}			
			}

			$qp = DB::getInstance()->get("payments_settings", "*", ["id" => 1]);
			if ($qp->count()) {
				foreach($qp->results() as $rp) {
					$currency = $rp->currency;
				}			
			}

			$qc = DB::getInstance()->get("currency", "*", ["id" => $currency]);
			if ($qc->count()) {
				foreach($qc->results() as $rc) {
					$currency_symbol = $rc->currency_symbol;
				}			
			}	 

			$time = ago(strtotime($row->date_added));	
			$blurb = truncateHtml($row->description, 250);

			$comList .= '

			<div class="job">	

			<div class="row top-sec">
			<div class="col-lg-12">
			<div class="col-lg-2 col-xs-12">
			<a href="freelancer.php?a=overview&&id='.$row->freelancerid.'" target="_blank">
			<img class="img-responsive" src="Freelancer/'.$imagelocation_3.'" alt="">
			</a>
			</div><!-- /.col-lg-2 -->
			<div class="col-lg-10 col-xs-12"> 
			<h4><a href="freelancer.php?a=overview&&id='.$row->freelancerid.'" target="_blank">'.$name_3.'</a></h4>
			<h5><i class="fa fa-at"></i> '.$username_3.'</h5>
			</div><!-- /.col-lg-10 -->

			</div><!-- /.col-lg-12 -->
			</div><!-- /.row -->

			<div class="row mid-sec">			 
			<div class="col-lg-12">			 
			<div class="col-lg-12">
			<hr class="small-hr">
			<p>'.$blurb.'</p>

			</div><!-- /.col-lg-12 -->
			</div><!-- /.col-lg-12 -->
			</div><!-- /.row -->

			<div class="row bottom-sec">
			<div class="col-lg-12">

			<div class="col-lg-12">
			<hr class="small-hr">
			</div> 

			<div class="col-lg-3">
			<h5>Posted </h5>
			<p> '.$time.'</p>
			</div>
			<div class="col-lg-3">
			<h5> Budget </h5>
			<p>'.$currency_symbol.' '.$row->budget.'</p>
			</div>

			</div><!-- /.col-lg-12 -->
			</div><!-- /.row -->

			</div><!-- /.job -->
			';
			$x++;		 
		}

	}

	if($loadmore) {
		$load = '<div class="text-center" id="more_comments_'.$row->id.'">

		<a class="btn text-center" onclick="loadProposals('.$row->id.', '.$jobid.', '.$limit.')">
		<hr class="star-light-left">
		<p>View More Proposals</p>
		<hr class="star-light-right">
		</a>
		</div>';
	}	 

	return $comList.$load;	
}

function getAllProposals($id, $jobid, $limit) {
	error_reporting(0);
	$comList = '';
	$c_per_page = $limit;
	
	$q1 = DB::getInstance()->get("proposal", "*", ["AND" => ["id[<]" => $id, "jobid" => $jobid, ]]);
		// If there are more results available than the limit, then show the Load More Comments
	$count = $q1->count();
	if($count > $c_per_page) {
		$loadmore = 1;
	} else {
		$loadmore = 0;
	}		

	$query = DB::getInstance()->get("proposal", "*", ["ORDER" => "id DESC", "AND" => ["id[<]" => $id, "jobid" => $jobid, ], "LIMIT" => $c_per_page]);
	if($query->count()) {
		$x = 1;	

		foreach($query->results() as $row) {

			$q3 = DB::getInstance()->get("freelancer", "*", ["freelancerid" => $row->freelancerid, "LIMIT" => 1]);
			if($q3->count()) {
				foreach($q3->results() as $r3) {
					$name_3 = $r3->name;
					$username_3 = $r3->username;
					$imagelocation_3 = $r3->imagelocation;
				}
			}		

			$q4 = DB::getInstance()->get("profile", "*", ["userid" => $row->freelancerid, "LIMIT" => 1]);
			if ($q4->count()) {
				foreach($q4->results() as $r4) {
					$country = $r4->country;
				}			
			} 	 

			foreach ($arr as $key => $value) {
				$skill .= '<label class="label label-success">'. $value .'</label> &nbsp;'; 
			}


	//Get Payments Settings Data
			$qp = DB::getInstance()->get("payments_settings", "*", ["id" => 1]);
			if ($qp->count()) {
				foreach($qp->results() as $rp) {
					$currency = $rp->currency;
				}			
			}

	//Get Payments Settings Data
			$qc = DB::getInstance()->get("currency", "*", ["id" => $currency]);
			if ($qc->count()) {
				foreach($qc->results() as $rc) {
					$currency_symbol = $rc->currency_symbol;
				}			
			}	

			$time = ago(strtotime($row->date_added));	
			$blurb = truncateHtml($row->description, 250);

			$comList .= '

			<div class="job">	

			<div class="row top-sec">
			<div class="col-lg-12">
			<div class="col-lg-2 col-xs-12">
			<a href="freelancer.php?a=overview&&id='.$row->freelancerid.'" target="_blank">
			<img class="img-responsive" src="Freelancer/'.$imagelocation_3.'" alt="">
			</a>
			</div><!-- /.col-lg-2 -->
			<div class="col-lg-10 col-xs-12"> 
			<h4><a href="freelancer.php?a=overview&&id='.$row->freelancerid.'" target="_blank">'.$name_3.'</a></h4>
			<h5><i class="fa fa-at"></i> '.$username_3.'</h5>
			</div><!-- /.col-lg-10 -->

			</div><!-- /.col-lg-12 -->
			</div><!-- /.row -->

			<div class="row mid-sec">			 
			<div class="col-lg-12">			 
			<div class="col-lg-12">
			<hr class="small-hr">
			<p>'.$blurb.'</p>

			</div><!-- /.col-lg-12 -->
			</div><!-- /.col-lg-12 -->
			</div><!-- /.row -->

			<div class="row bottom-sec">
			<div class="col-lg-12">

			<div class="col-lg-12">
			<hr class="small-hr">
			</div> 

			<div class="col-lg-3">
			<h5>Posted </h5>
			<p> '.$time.'</p>
			</div>
			<div class="col-lg-3">
			<h5> Location </h5>
			<p><i class="fa fa-map-marker"></i> '.$country.'</p>
			</div>
			<div class="col-lg-3">
			<h5> Budget </h5>
			<p>'.$currency_symbol.' '.$row->budget.'</p>
			</div>

			</div><!-- /.col-lg-12 -->
			</div><!-- /.row -->

			</div><!-- /.job -->
			';
			$x++;		 
		}

	}

	if($loadmore) {
		$load = '<div class="text-center" id="more_comments_'.$row->id.'">

		<a class="btn text-center" onclick="loadProposals('.$row->id.', '.$jobid.', '.$limit.')">
		<hr class="star-light-left">
		<p>View More Proposals</p>
		<hr class="star-light-right">
		</a>
		</div>';
	}	 

	return $comList.$load;	
}	

//Get Featured Proposals
function getFeaturedProposals($jobid) {
	
	$comList = '';
	$query = DB::getInstance()->get("proposal", "*", ["ORDER" => "date_added DESC", "AND" => ["jobid" => $jobid, ]]);
	if($query->count()) {
		$x = 1;	
		
		foreach($query->results() as $row) {	

			$q3 = DB::getInstance()->get("freelancer", "*", ["freelancerid" => $row->freelancerid, "LIMIT" => 1]);
			if($q3->count()) {
				foreach($q3->results() as $r3) {
					$name_3 = $r3->name;
					$username_3 = $r3->username;
					$imagelocation_3 = $r3->imagelocation;
				}
			}		


			$qp = DB::getInstance()->get("payments_settings", "*", ["id" => 1]);
			if ($qp->count()) {
				foreach($qp->results() as $rp) {
					$currency = $rp->currency;
				}			
			}

			$qc = DB::getInstance()->get("currency", "*", ["id" => $currency]);
			if ($qc->count()) {
				foreach($qc->results() as $rc) {
					$currency_symbol = $rc->currency_symbol;
				}			
			}	 

			$time = ago(strtotime($row->date_added));	
			$blurb = truncateHtml($row->description, 250);

			$comList .= '

			<div class="job">	

			<div class="row top-sec">
			<div class="col-lg-12">
			<div class="col-lg-2 col-xs-12">
			<a href="freelancer.php?a=overview&&id='.$row->freelancerid.'" target="_blank">
			<img class="img-responsive" src="Freelancer/'.$imagelocation_3.'" alt="">
			</a>
			</div><!-- /.col-lg-2 -->
			<div class="col-lg-10 col-xs-12"> 
			<h4><a href="freelancer.php?a=overview&&id='.$row->freelancerid.'" target="_blank">'.$name_3.'</a> <label class="label bg-danger m-left"> Featured Proposal </label></h4>
			<h5><i class="fa fa-at"></i> '.$username_3.'</h5>
			</div><!-- /.col-lg-10 -->

			</div><!-- /.col-lg-12 -->
			</div><!-- /.row -->

			<div class="row mid-sec">			 
			<div class="col-lg-12">			 
			<div class="col-lg-12">
			<hr class="small-hr">
			<p>'.$blurb.'</p>

			</div><!-- /.col-lg-12 -->
			</div><!-- /.col-lg-12 -->
			</div><!-- /.row -->

			<div class="row bottom-sec">
			<div class="col-lg-12">

			<div class="col-lg-12">
			<hr class="small-hr">
			</div> 

			<div class="col-lg-3">
			<h5>Posted </h5>
			<p> '.$time.'</p>
			</div>
			<div class="col-lg-3">
			<h5> Budget </h5>
			<p>'.$currency_symbol.' '.$row->budget.'</p>
			</div>
			</div><!-- /.col-lg-12 -->
			</div><!-- /.row -->

			</div><!-- /.job -->
			';
			$x++;		 
		}
	}
	return $comList;	
}

//Discussions Functions
function getAdminDiscussion($freelancerid, $clientid) {
	$comList = '';
	$c_per_page = 2;
	var_dump("expression");
	
	$query = DB::getInstance()->get("message", "*", [	
		"ORDER" => "date_added DESC",
		'AND' => [
			'discid' => $_GET['id'],
			"OR" => [
				"AND #first" => [
					"user_from" => $clientid,
					"user_to" => $freelancerid,
					"disc" => 1
				],
				"AND #second" => [
					"user_from" => $freelancerid,
					"user_to" => $clientid,
					"disc" => 1
				]
			]
		]]);
	if($query->count()) {
		$x = 1;	
		
		foreach($query->results() as $row) {


			$q3 = DB::getInstance()->get("freelancer", "*", ["freelancerid" => $freelancerid, "LIMIT" => 1]);
			if($q3->count()) {
				foreach($q3->results() as $r3) {
					$name_3 = $r3->name;
					$imagelocation_3 = $r3->imagelocation;
				}
			}	

			$q4 = DB::getInstance()->get("client", "*", ["clientid" => $clientid, "LIMIT" => 1]);
			if($q4->count()) {
				foreach($q4->results() as $r4) {
					$name_4 = $r4->name;
					$imagelocation_4 = $r4->imagelocation;
				}
			}	 			

			$time = ago(strtotime($row->date_added));	

			if ($freelancerid === $row->user_from) {
				$comList .= '
				<section id="comment'.$row->id.'" class="comment-list block">
				<article class="comment-item">
				<a class="pull-left thumb-sm avatar">
				<img src="../Freelancer/' . $imagelocation_3 . '" class="img-circle" alt="...">
				</a>
				<span class="arrow left"></span>
				<section class="comment-body panel panel-default bg-white">
				<header class="panel-heading">
				<a href="../freelancer.php?a=overview&id=' . $freelancerid . '" target="_blank">' . $name_3 . ' 
				<label class="label bg-danger m-left"> Freelancer </label></a>
				&nbsp; <span class="text-center"><i class="fa fa-clock-o"></i> ' . $time . ' </span>
				<span class="text-muted m-l-sm pull-right">
				</header>
				<div class="panel-body">
				' . $row->message . '
				</div>
				</section>
				</article><!-- .message-end -->
				</section>

				';		

			}
			if ($clientid === $row->user_from) {

				$comList .= '
				<section id="comment'.$row->id.'" class="comment-list block">
				<article class="comment-item">
				<a class="pull-left thumb-sm avatar">
				<img src="../Client/' . $imagelocation_4 . '" class="img-circle" alt="...">
				</a>
				<span class="arrow left"></span>
				<section class="comment-body panel panel-default bg-white">
				<header class="panel-heading">
				<a href="../client.php?a=overview&id=' . $clientid . '" target="_blank">' . $name_4 . ' 
				<label class="label bg-info m-left"> Client </label></a>
				&nbsp; <i class="fa fa-clock-o"></i> ' . $time . ' </span>
				<span class="text-muted m-l-sm pull-right">
				</header>
				<div class="panel-body">
				' . $row->message . '
				</div>
				</section>
				</article><!-- .message-end -->
				</section>

				';		
			}
			$x++;		 
		}

	}

	return $comList;	
}


function getDiscussion($freelancerid, $clientid) {
	$comList = '';
	$c_per_page = 2;
	
	
	
	$query = DB::getInstance()->get("message", "*", [	
		"ORDER" => "date_added DESC",
		'AND' => [
			'discid' => $_GET['id'],
			"OR" => [
				"AND #first" => [
					"user_from" => $clientid,
					"user_to" => $freelancerid,
					"disc" => 1
				],
				"AND #second" => [
					"user_from" => $freelancerid,
					"user_to" => $clientid,
					"disc" => 1
				]
			]
		]]);
	if($query->count()) {
		$x = 1;	
		
		foreach($query->results() as $row) {


			$q3 = DB::getInstance()->get("freelancer", "*", ["freelancerid" => $freelancerid, "LIMIT" => 1]);
			if($q3->count()) {
				foreach($q3->results() as $r3) {
					$name_3 = $r3->name;
					$imagelocation_3 = $r3->imagelocation;
				}
			}	

			$q4 = DB::getInstance()->get("client", "*", ["clientid" => $clientid, "LIMIT" => 1]);
			if($q4->count()) {
				foreach($q4->results() as $r4) {
					$name_4 = $r4->name;
					$imagelocation_4 = $r4->imagelocation;
				}
			}	 			

			$time = ago(strtotime($row->date_added));	

			if ($freelancerid === $row->user_from) {

				if($row->user_from == $clientid) {
					$delete = '<a class="btn" onclick="delete_the('.$row->id.')" title="Delete Message"><i class="fa fa-trash-o"></i></a>';
				} else {
					$delete = '';
				}		
				
				$comList .= '
				<section id="comment'.$row->id.'" class="comment-list block">
				<article class="comment-item">
				<a class="pull-left thumb-sm avatar">
				<img src="../Freelancer/' . $imagelocation_3 . '" class="img-circle" alt="...">
				</a>
				<span class="arrow left"></span>
				<section class="comment-body panel panel-default bg-white">
				<header class="panel-heading">
				<a href="../freelancer.php?a=overview&id=' . $freelancerid . '" target="_blank">' . $name_3 . ' 
				<label class="label bg-danger m-left"> Freelancer </label></a>
				&nbsp; <span class="text-center"><i class="fa fa-clock-o"></i> ' . $time . ' </span>
				<span class="text-muted m-l-sm pull-right">
				' . $delete . '
				</header>
				<div class="panel-body">
				' . $row->message . '
				</div>
				</section>
				</article><!-- .message-end -->
				</section>

				';		

			}
			if ($clientid === $row->user_from) {

				if($row->user_from == $clientid) {
					$delete = '<a class="btn" onclick="delete_the('.$row->id.', '.$clientid.')" title="Delete Message"><i class="fa fa-trash-o"></i></a>';
				} else {
					$delete = '';
				}		

				$comList .= '
				<section id="comment'.$row->id.'" class="comment-list block">
				<article class="comment-item">
				<a class="pull-left thumb-sm avatar">
				<img src="../Client/' . $imagelocation_4 . '" class="img-circle" alt="...">
				</a>
				<span class="arrow left"></span>
				<section class="comment-body panel panel-default bg-white">
				<header class="panel-heading">
				<a href="../client.php?a=overview&id=' . $clientid . '" target="_blank">' . $name_4 . ' 
				<label class="label bg-info m-left"> Client </label></a>
				&nbsp; <i class="fa fa-clock-o"></i> ' . $time . ' </span>
				<span class="text-muted m-l-sm pull-right">
				' . $delete . '
				</header>
				<div class="panel-body">
				' . $row->message . '
				</div>
				</section>
				</article><!-- .message-end -->
				</section>
				
				';		
			}
			$x++;		 
		}

	}

	return $comList;	
}

function getLastDiscussion($clientid, $freelancerid) {
	$Last = '';
	$query = DB::getInstance()->get("message", "*", ["LIMIT" => [0,1], "AND" => ["user_to" => $freelancerid, "user_from" => $clientid, "disc" => 1], "ORDER" => "id DESC"]);
	if($query->count()) {
		$x = 1;
		
		foreach($query->results() as $row) {

			$q3 = DB::getInstance()->get("client", "*", ["clientid" => $row->user_from, "LIMIT" => 1]);
			if($q3->count()) {
				foreach($q3->results() as $r3) {
					$name_3 = $r3->name;
					$imagelocation_3 = $r3->imagelocation;
				}
			}		

			if($row->user_from == $clientid) {
				$delete = '<a class="btn" onclick="delete_the('.$row->id.', '.$clientid.')" title="Delete Comment"><i class="fa fa-trash-o"></i></a>';
			} else {
				$delete = '';
			}	

			$time = ago(strtotime($row->date_added));	


			$Last .= '

			<section id="comment'.$row->id.'" class="comment-list block">
			<article class="comment-item">
			<a class="pull-left thumb-sm avatar">
			<img src="' . $imagelocation_3 . '" class="img-circle" alt="...">
			</a>
			<span class="arrow left"></span>
			<section class="comment-body panel panel-default bg-white">
			<header class="panel-heading">
			<a href="../client.php?a=overview&id=' . $clientid . '" target="_blank">' . $name_3 . ' 
			<label class="label bg-info m-left"> Client </label></a>
			&nbsp; <i class="fa fa-clock-o"></i> ' . $time . ' </span>
			<span class="text-muted m-l-sm pull-right">
			' . $delete . '
			</header>
			<div class="panel-body">
			' . $row->message . '
			</div>
			</section>
			</article><!-- .message-end -->
			</section>

			';
			$x++;		 
		}

	}

	return $Last;	
	unset($Last);
}

function getDiscussionF($clientid, $freelancerid) {
	$comList = '';
	$c_per_page = 2;
	
	
	
	$query = DB::getInstance()->get("message", "*", [	
		"ORDER" => "date_added DESC",
		'AND' => [
			'discid' => $_GET['id'],
			"OR" => [
				"AND #first" => [
					"user_from" => $clientid,
					"user_to" => $freelancerid,
					"disc" => 1
				],
				"AND #second" => [
					"user_from" => $freelancerid,
					"user_to" => $clientid,
					"disc" => 1
				]
			]
		]]);
	if($query->count()) {
		$x = 1;	
		
		foreach($query->results() as $row) {

			$q3 = DB::getInstance()->get("freelancer", "*", ["freelancerid" => $freelancerid, "LIMIT" => 1]);
			if($q3->count()) {
				foreach($q3->results() as $r3) {
					$name_3 = $r3->name;
					$imagelocation_3 = $r3->imagelocation;
				}
			}	

			$q4 = DB::getInstance()->get("client", "*", ["clientid" => $clientid, "LIMIT" => 1]);
			if($q4->count()) {
				foreach($q4->results() as $r4) {
					$name_4 = $r4->name;
					$imagelocation_4 = $r4->imagelocation;
				}
			}	 			
			$time = ago(strtotime($row->date_added));	

			if ($freelancerid === $row->user_from) {

				if($row->user_from == $freelancerid) { 
					$delete = '<a class="btn" onclick="delete_the('.$row->id.', '.$freelancerid.')" title="Delete Message"><i class="fa fa-trash-o"></i></a>';
				} else {
					$delete = '';
				}

				$comList .= '
				<section id="comment'.$row->id.'" class="comment-list block">
				<article class="comment-item">
				<a class="pull-left thumb-sm avatar">
				<img src="../Freelancer/' . $imagelocation_3 . '" class="img-circle" alt="...">
				</a>
				<span class="arrow left"></span>
				<section class="comment-body panel panel-default bg-white">
				<header class="panel-heading">
				<a href="../freelancer.php?a=overview&id=' . $freelancerid . '" target="_blank">' . $name_3 . ' 
				<label class="label bg-danger m-left"> Freelancer </label>
				</a>
				&nbsp; <i class="fa fa-clock-o"></i> ' . $time . ' </span>
				<span class="text-muted m-l-sm pull-right">
				' . $delete . '
				</header>
				<div class="panel-body">
				' . $row->message . '
				</div>
				</section>
				</article><!-- .message-end -->
				</section>

				';		

			}
			if ($clientid === $row->user_from) {

				if($row->user_from == $freelancerid) {
					$delete = '<a class="btn" onclick="delete_the('.$row->id.', '.$freelancerid.')" title="Delete Message"><i class="fa fa-trash-o"></i></a>';
				} else {
					$delete = '';
				}		

				$comList .= '
				<section id="comment'.$row->id.'" class="comment-list block">
				<article class="comment-item">
				<a class="pull-left thumb-sm avatar">
				<img src="../Client/' . $imagelocation_4 . '" class="img-circle" alt="...">
				</a>
				<span class="arrow left"></span>
				<section class="comment-body panel panel-default bg-white">
				<header class="panel-heading">
				<a href="../client.php?a=overview&id=' . $clientid . '" target="_blank">' . $name_4 . '
				<label class="label bg-info m-left"> Client </label>
				</a>
				&nbsp; <i class="fa fa-clock-o"></i> ' . $time . ' </span>
				<span class="text-muted m-l-sm pull-right">
				' . $delete . '
				</header>
				<div class="panel-body">
				' . $row->message . '
				</div>
				</section>
				</article><!-- .message-end -->
				</section>

				';		
			}
			$x++;		 
		}
	}

	return $comList;	
}

function getLastDiscussionF($freelancerid, $clientid) {
	$Last = '';
	$query = DB::getInstance()->get("message", "*", ["LIMIT" => [0,1], "AND" => ["user_to" => $clientid, "user_from" => $freelancerid, "disc" => 1], "ORDER" => "id DESC"]);
	if($query->count()) {
		$x = 1;
		
		foreach($query->results() as $row) {
			
			$q3 = DB::getInstance()->get("freelancer", "*", ["freelancerid" => $row->user_from, "LIMIT" => 1]);
			if($q3->count()) {
				foreach($q3->results() as $r3) {
					$name_3 = $r3->name;
					$imagelocation_3 = $r3->imagelocation;
				}
			}		

			if($row->user_from == $freelancerid) {
				$delete = '<a class="btn" onclick="delete_the('.$row->id.', '.$freelancerid.')" title="Delete Comment"><i class="fa fa-trash-o"></i></a>';
			} else {
				$delete = '';
			}	

			$time = ago(strtotime($row->date_added));	


			$Last .= '

			<section id="comment'.$row->id.'" class="comment-list block">
			<article class="comment-item">
			<a class="pull-left thumb-sm avatar">
			<img src="' . $imagelocation_3 . '" class="img-circle" alt="...">
			</a>
			<span class="arrow left"></span>
			<section class="comment-body panel panel-default bg-white">
			<header class="panel-heading">
			<a href="../freelancer.php?a=overview&id=' . $freelancerid . '" target="_blank">' . $name_3 . ' 
			<label class="label bg-danger m-left"> Freelancer </label>
			</a>
			&nbsp; <i class="fa fa-clock-o"></i> ' . $time . ' </span>
			<span class="text-muted m-l-sm pull-right">
			' . $delete . '
			</header>
			<div class="panel-body">
			' . $row->message . '
			</div>
			</section>
			</article><!-- .message-end -->
			</section>

			';
			$x++;		 
		}

	}

	return $Last;	
	unset($Last);
}

function getCommentF($clientid, $freelancerid, $bugid) {
	$comList = '';
	$c_per_page = 2;
	
	
	
	$query = DB::getInstance()->get("message", "*", [	
		"ORDER" => "date_added DESC",
		"OR" => [
			"AND #first" => [
				"user_from" => $clientid,
				"user_to" => $freelancerid,
				"delete_remove" => 0,
				"bugid" => $bugid,
				"disc" => 2
			],
			"AND #second" => [
				"user_from" => $freelancerid,
				"user_to" => $clientid,
				"delete_remove" => 0,
				"bugid" => $bugid,
				"disc" => 2
			]
		]]);
	if($query->count()) {
		$x = 1;	
		
		foreach($query->results() as $row) {

			$q5 = DB::getInstance()->get("trash", "*", ["AND" => ["messageid" => $row->id, "userid" => $freelancerid], "LIMIT" => 1]);
			if ($q5->count() === 0) {



				$q3 = DB::getInstance()->get("freelancer", "*", ["freelancerid" => $freelancerid, "LIMIT" => 1]);
				if($q3->count()) {
					foreach($q3->results() as $r3) {
						$name_3 = $r3->name;
						$imagelocation_3 = $r3->imagelocation;
					}
				}	

				$q4 = DB::getInstance()->get("client", "*", ["clientid" => $clientid, "LIMIT" => 1]);
				if($q4->count()) {
					foreach($q4->results() as $r4) {
						$name_4 = $r4->name;
						$imagelocation_4 = $r4->imagelocation;
					}
				}	 			

				$time = ago(strtotime($row->date_added));	

				if ($freelancerid === $row->user_from) {

					if($row->user_from == $freelancerid) {
						$delete = '<a class="btn" onclick="delete_the('.$row->id.', '.$freelancerid.')" title="Delete Message"><i class="fa fa-trash-o"></i></a>';
					} else {
						$delete = '';
					}		
					
					$comList .= '
					<section id="comment'.$row->id.'" class="comment-list block">
					<article class="comment-item">
					<a class="pull-left thumb-sm avatar">
					<img src="../Freelancer/' . $imagelocation_3 . '" class="img-circle" alt="...">
					</a>
					<span class="arrow left"></span>
					<section class="comment-body panel panel-default bg-white">
					<header class="panel-heading">
					<a href="../freelancer.php?a=overview&id=' . $freelancerid . '" target="_blank">' . $name_3 . ' 
					<label class="label bg-danger m-left"> Freelancer </label>
					</a>
					&nbsp; <i class="fa fa-clock-o"></i> ' . $time . ' </span>
					<span class="text-muted m-l-sm pull-right">
					' . $delete . '
					</header>
					<div class="panel-body">
					' . $row->message . '
					</div>
					</section>
					</article><!-- .message-end -->
					</section>

					';		

				}
				if ($clientid === $row->user_from) {

					if($row->user_from == $freelancerid) {
						$delete = '<a class="btn" onclick="delete_the('.$row->id.', '.$freelancerid.')" title="Delete Message"><i class="fa fa-trash-o"></i></a>';
					} else {
						$delete = '';
					}		

					$comList .= '
					<section id="comment'.$row->id.'" class="comment-list block">
					<article class="comment-item">
					<a class="pull-left thumb-sm avatar">
					<img src="../Client/' . $imagelocation_4 . '" class="img-circle" alt="...">
					</a>
					<span class="arrow left"></span>
					<section class="comment-body panel panel-default bg-white">
					<header class="panel-heading">
					<a href="../client.php?a=overview&id=' . $clientid . '" target="_blank">' . $name_4 . '
					<label class="label bg-info m-left"> Client </label>
					</a>
					&nbsp; <i class="fa fa-clock-o"></i> ' . $time . ' </span>
					<span class="text-muted m-l-sm pull-right">
					' . $delete . '
					</header>
					<div class="panel-body">
					' . $row->message . '
					</div>
					</section>
					</article><!-- .message-end -->
					</section>

					';		
				}
				$x++;		 
			}
		}

	}

	return $comList;	
}

function getLastCommentF($freelancerid, $clientid , $bugid) {
	$Last = '';
	$query = DB::getInstance()->get("message", "*", ["LIMIT" => [0,1], "AND" => ["user_to" => $clientid, "user_from" => $freelancerid, "bugid" => $bugid, "disc" => 2], "ORDER" => "id DESC"]);
	if($query->count()) {
		$x = 1;
		
		foreach($query->results() as $row) {

			$q3 = DB::getInstance()->get("freelancer", "*", ["freelancerid" => $row->user_from, "LIMIT" => 1]);
			if($q3->count()) {
				foreach($q3->results() as $r3) {
					$name_3 = $r3->name;
					$imagelocation_3 = $r3->imagelocation;
				}
			}		

			if($row->user_from == $freelancerid) {
				$delete = '<a class="btn" onclick="delete_the('.$row->id.', '.$freelancerid.')" title="Delete Comment"><i class="fa fa-trash-o"></i></a>';
			} else {
				$delete = '';
			}	

			$time = ago(strtotime($row->date_added));	


			$Last .= '

			<section id="comment'.$row->id.'" class="comment-list block">
			<article class="comment-item">
			<a class="pull-left thumb-sm avatar">
			<img src="' . $imagelocation_3 . '" class="img-circle" alt="...">
			</a>
			<span class="arrow left"></span>
			<section class="comment-body panel panel-default bg-white">
			<header class="panel-heading">
			<a href="../freelancer.php?a=overview&id=' . $freelancerid . '" target="_blank">' . $name_3 . ' 
			<label class="label bg-danger m-left"> Freelancer </label>
			</a>
			&nbsp; <i class="fa fa-clock-o"></i> ' . $time . ' </span>
			<span class="text-muted m-l-sm pull-right">
			' . $delete . '
			</header>
			<div class="panel-body">
			' . $row->message . '
			</div>
			</section>
			</article><!-- .message-end -->
			</section>

			';
			$x++;		 
		}

	}

	return $Last;	
	unset($Last);
}

function getComment($freelancerid, $clientid, $bugid) {
	$comList = '';
	$c_per_page = 2;
	
	
	
	$query = DB::getInstance()->get("message", "*", [	
		"ORDER" => "date_added DESC",
		"OR" => [
			"AND #first" => [
				"user_from" => $clientid,
				"user_to" => $freelancerid,
				"delete_remove" => 0,
				"bugid" => $bugid,
				"disc" => 2
			],
			"AND #second" => [
				"user_from" => $freelancerid,
				"user_to" => $clientid,
				"delete_remove" => 0,
				"bugid" => $bugid,
				"disc" => 2
			]
		]]);
	if($query->count()) {
		$x = 1;	
		
		foreach($query->results() as $row) {

			$q5 = DB::getInstance()->get("trash", "*", ["AND" => ["messageid" => $row->id, "userid" => $freelancerid], "LIMIT" => 1]);
			if ($q5->count() === 0) {



				$q3 = DB::getInstance()->get("freelancer", "*", ["freelancerid" => $freelancerid, "LIMIT" => 1]);
				if($q3->count()) {
					foreach($q3->results() as $r3) {
						$name_3 = $r3->name;
						$imagelocation_3 = $r3->imagelocation;
					}
				}	

				$q4 = DB::getInstance()->get("client", "*", ["clientid" => $clientid, "LIMIT" => 1]);
				if($q4->count()) {
					foreach($q4->results() as $r4) {
						$name_4 = $r4->name;
						$imagelocation_4 = $r4->imagelocation;
					}
				}	 			

				$time = ago(strtotime($row->date_added));	

				if ($freelancerid === $row->user_from) {

					if($row->user_from == $clientid) {
						$delete = '<a class="btn" onclick="delete_the('.$row->id.', '.$clientid.')" title="Delete Message"><i class="fa fa-trash-o"></i></a>';
					} else {
						$delete = '';
					}		
					
					$comList .= '
					<section id="comment'.$row->id.'" class="comment-list block">
					<article class="comment-item">
					<a class="pull-left thumb-sm avatar">
					<img src="../Freelancer/' . $imagelocation_3 . '" class="img-circle" alt="...">
					</a>
					<span class="arrow left"></span>
					<section class="comment-body panel panel-default bg-white">
					<header class="panel-heading">
					<a href="../freelancer.php?a=overview&id=' . $freelancerid . '" target="_blank">' . $name_3 . ' 
					<label class="label bg-danger m-left"> Freelancer </label>
					</a>
					&nbsp; <i class="fa fa-clock-o"></i> ' . $time . ' </span>
					<span class="text-muted m-l-sm pull-right">
					' . $delete . '
					</header>
					<div class="panel-body">
					' . $row->message . '
					</div>
					</section>
					</article><!-- .message-end -->
					</section>

					';		

				}
				if ($clientid === $row->user_from) {

					if($row->user_from == $clientid) {
						$delete = '<a class="btn" onclick="delete_the('.$row->id.', '.$clientid.')" title="Delete Message"><i class="fa fa-trash-o"></i></a>';
					} else {
						$delete = '';
					}		

					$comList .= '
					<section id="comment'.$row->id.'" class="comment-list block">
					<article class="comment-item">
					<a class="pull-left thumb-sm avatar">
					<img src="../Client/' . $imagelocation_4 . '" class="img-circle" alt="...">
					</a>
					<span class="arrow left"></span>
					<section class="comment-body panel panel-default bg-white">
					<header class="panel-heading">
					<a href="../client.php?a=overview&id='. $clientid .'" target="_blank">' . $name_4 . '
					<label class="label bg-info m-left"> Client </label>
					</a>
					&nbsp; <i class="fa fa-clock-o"></i> ' . $time . ' </span>
					<span class="text-muted m-l-sm pull-right">
					' . $delete . '
					</header>
					<div class="panel-body">
					' . $row->message . '
					</div>
					</section>
					</article><!-- .message-end -->
					</section>

					';		
				}
				$x++;		 
			}
		}

	}

	return $comList;	
}

function getLastComment($clientid, $freelancerid, $bugid) {
	$Last = '';
	$query = DB::getInstance()->get("message", "*", ["LIMIT" => [0,1], "AND" => ["user_to" => $freelancerid, "user_from" => $clientid, "bugid" => $bugid, "disc" => 2], "ORDER" => "id DESC"]);
	if($query->count()) {
		$x = 1;
		
		foreach($query->results() as $row) {
			
			$q3 = DB::getInstance()->get("client", "*", ["clientid" => $row->user_from, "LIMIT" => 1]);
			if($q3->count()) {
				foreach($q3->results() as $r3) {
					$name_3 = $r3->name;
					$imagelocation_3 = $r3->imagelocation;
				}
			}			

			if($row->user_from == $clientid) {
				$delete = '<a class="btn" onclick="delete_the('.$row->id.', '.$clientid.')" title="Delete Comment"><i class="fa fa-trash-o"></i></a>';
			} else {
				$delete = '';
			}	

			$time = ago(strtotime($row->date_added));	


			$Last .= '

			<section id="comment'.$row->id.'" class="comment-list block">
			<article class="comment-item">
			<a class="pull-left thumb-sm avatar">
			<img src="' . $imagelocation_3 . '" class="img-circle" alt="...">
			</a>
			<span class="arrow left"></span>
			<section class="comment-body panel panel-default bg-white">
			<header class="panel-heading">
			<a href="../client.php?a=overview&id='. $clientid .'" target="_blank">' . $name_3 . ' 
			<label class="label bg-info m-left"> Client </label>
			</a>
			&nbsp; <i class="fa fa-clock-o"></i> ' . $time . ' </span>
			<span class="text-muted m-l-sm pull-right">
			' . $delete . '
			</header>
			<div class="panel-body">
			' . $row->message . '
			</div>
			</section>
			</article><!-- .message-end -->
			</section>

			';
			$x++;		 
		}

	}

	return $Last;	
	unset($Last);
}
?>