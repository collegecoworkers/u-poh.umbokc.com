<?php

//Get Site Settings Data
$query = DB::getInstance()->get("settings", "*", ["id" => 1]);
if ($query->count()) {
	foreach($query->results() as $row) {
		$sid = $row->id;
		$title = $row->title;
		$use_icon = $row->use_icon;
		$site_icon = $row->site_icon;
		$tagline = $row->tagline;
		$url = $row->url;
		$mail = $row->mail;
		$mailpass = $row->mailpass;
		$job_limit = $row->job_limit;
		$service_limit = $row->service_limit;
		$proposal_limit = $row->proposal_limit;
		$top_title = $row->top_title;
		$show_downtitle = $row->show_downtitle;
		$down_title = $row->down_title;
		$searchterm = $row->searchterm;
		$header_img = $row->header_img;
		$cattagline = $row->cattagline;
		$services_header_img = $row->services_header_img;
		$jobs_header_img = $row->jobs_header_img;
	}
}

//Get Payments Settings Data
$q1 = DB::getInstance()->get("payments_settings", "*", ["id" => 1]);
if ($q1->count()) {
	foreach($q1->results() as $r1) {
		$currency = $r1->currency;
		$membershipid = $r1->membershipid;
	}			
}

//Get Payments Settings Data
$q2 = DB::getInstance()->get("currency", "*", ["id" => $currency]);
if ($q2->count()) {
	foreach($q2->results() as $r2) {
		$currency_symbol = $r2->currency_symbol;
	}			
}

?>
<head>
			<!-- ==============================================
		Title and Meta Tags
		=============================================== -->
		<meta charset="utf-8">
		<title><?php echo escape($title) .' - '. escape($tagline) ; ?></title>
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<link rel="shortcut icon" href="img/favicons/favicon.ico">
		<link rel="apple-touch-icon" href="img/favicons/apple-touch-icon.png">
		<link rel="apple-touch-icon" sizes="72x72" href="img/favicons/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="114x114" href="img/favicons/apple-touch-icon-114x114.png">
		
			<!-- ==============================================
		CSS
		=============================================== -->
		<!-- Style-->
		<link href="assets/css/hopler.css" rel="stylesheet" type="text/css" />

		<!-- ==============================================
		Feauture Detection
		=============================================== -->
		<script src="/assets/js/modernizr-custom.js"></script>
		
		<!--[if lt IE 9]>
		 <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->		
		
	</head>